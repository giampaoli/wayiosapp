//
//  PhoneTextField.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 24/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class PhoneTextField: FormattedTextField {
    
    override func initialize() {
        super.initialize()
        self.formatRules = PhoneTextFieldFormatRules()
    }
}
