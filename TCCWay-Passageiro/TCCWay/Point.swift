//
//  Point.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 14/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import ObjectMapper
import MapKit

struct Point: Mappable {
    
    private(set) var latitude: Double = 0
    private(set) var longitude: Double = 0
    private(set) var location: CLLocationCoordinate2D? = nil
    
    init?(map: Map) {}
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
        
        if latitude != 0 && longitude != 0 {
            location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
    }
    
    init(location: CLLocationCoordinate2D ) {
        self.latitude = location.latitude
        self.longitude = location.longitude
        self.location = location
    }
    
    mutating func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        
        if latitude != 0 && longitude != 0 {
            location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
    }
}
