//
//  Token.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 25/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation

import ObjectMapper

struct User: Mappable {
    
    private(set) var id = 0
    private(set) var name = ""
    private(set) var birthDate = ""
    private(set) var email = ""
    private(set) var password = ""
    private(set) var confirmPassword = ""
    private(set) var cnh : CLong = 0
    private(set) var phone : CLong = 0
    private(set) var status = ""
    private(set) var insertDate = ""
    private(set) var url_imagem = ""
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- map["ID"]
        name <- map["NOME"]
        birthDate <- map["DATA_NASCIMENTO"]
        email <- map["email"]
        password <- map["password"]
        confirmPassword <- map["confirmPassword"]
        cnh <- map["cnh"]
        phone <- map["phone"]
        status <- map["status"]
        insertDate <- map["DATA_CRIACAO"]
        url_imagem <- map["URL_IMAGEM"]
    }
    
    static func login(json: String) {
        UserDefaults.standard.set(json, forKey: "User.current")
        UserDefaults.standard.synchronize()
    }
    
    static func logout() {
        UserDefaults.standard.removeObject(forKey: "User.current")
        UserDefaults.standard.synchronize()
    }
    
    static func user() -> User {
        let userString = UserDefaults.standard.string(forKey: "User.current")
        let user = User(JSONString: userString!)
        
        return user!
    }
    
    static func isLogged() -> Bool {
        var user: User? =  nil
        
        if let userString = UserDefaults.standard.string(forKey: "User.current") {
            user = User(JSONString: userString)
        }
        
        return user != nil
    }
}
