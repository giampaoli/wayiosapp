//
//  LineDetailsViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 04/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import MapKit
import Foundation

class LineDetailsViewController: UIViewController {
    
    @IBOutlet weak var imageVan: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var viewButtonPassenger: UIView!
    @IBOutlet weak var viewButtonTimeline: UIView!
    @IBOutlet weak var viewButtonInformations: UIView!
    
    var route = [MyAnnotation]()
    var routeNavigation = [CLLocationCoordinate2D]()
    var lineImage: UIImage? = nil
    var line: Line? = nil
    
    var realTimeAnnotation = MyAnnotation(title: "Van", id: "van", image: #imageLiteral(resourceName: "pin_van") , coordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0))
    
    weak var timer: Timer?
    var count = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startTimer()
        
        //TODO: Poderiamos não passar essa imagem assim e ja fazer download no momento do parse
        imageVan.image = lineImage
        
        setupMenuIntens()
        setupRoute()
        setupRouteNavigation()
        setupMap()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
        startTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController!.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController!.navigationBar.shadowImage = nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupNavigation() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = line?.title
    }
    
    func setupMenuIntens() {
        let gesturePassenger = UITapGestureRecognizer(target: self, action:  #selector (self.touchPassenger(sender:)))
        viewButtonPassenger.addGestureRecognizer(gesturePassenger)
        
        let gestureTimeline = UITapGestureRecognizer(target: self, action:  #selector (self.touchTimeline(sender:)))
        viewButtonTimeline.addGestureRecognizer(gestureTimeline)
        
        let gestureInformartions = UITapGestureRecognizer(target: self, action:  #selector (self.touchInformations(sender:)))
        viewButtonInformations.addGestureRecognizer(gestureInformartions)
        
    }
    
    func  touchPassenger(sender : UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "passenger", sender: nil)
    }
    
    func  touchTimeline(sender : UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "timeline", sender: nil)
    }
    
    func  touchInformations(sender : UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "status", sender: nil)
    }
    
    @IBAction func goValueChanged(_ sender: UISwitch) {
        
    }
    
    @IBAction func backValueChaged(_ sender: UISwitch) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "timeline", let nextScene = segue.destination as? LineNotificationsTableViewController {
            nextScene.lineId = (line?.idLine.description)!
        } else if segue.identifier == "passenger", let nextScene = segue.destination as? MembersTableViewController {
            nextScene.lineId = (line?.idLine.description)!
        } else if segue.identifier == "status", let nextScene = segue.destination as? StatusViewController {
            nextScene.lineId = (line?.idLine.description)!
        }
    }
}

extension LineDetailsViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        renderer.lineWidth = 3
        return renderer
    }
    
    func startTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true) { [weak self] _ in
            print("Start timer")
            self?.count = (self?.count)!  + 1
            
            self?.moveAnnotation(nextCoordinate: (self?.routeNavigation[(self?.count)!])!)
            
            if self?.routeNavigation == nil {
                self!.stopTimer()
            } else if self!.count == (self!.routeNavigation.count) - 1 {
                self!.stopTimer()
            }
        }
    }
    
    func stopTimer() {
        timer?.invalidate()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        stopTimer()
    }
    
    func moveAnnotation(nextCoordinate: CLLocationCoordinate2D) {
        UIView.animate(withDuration: 5.0, delay: 7.0, options: .curveLinear, animations: {() -> Void in
            print("next")
            print(nextCoordinate)
            self.mapView.removeAnnotation(self.realTimeAnnotation)
            self.realTimeAnnotation.coordinate = nextCoordinate
            self.mapView.addAnnotation(self.realTimeAnnotation)
            self.mapView.zoomingPoint(nextCoordinate)
        }) { _ in }
    }
    
    func setupMap() {
        let initialLocation = CLLocation(latitude: (route.first!.coordinate.latitude), longitude: (route.first!.coordinate.longitude))
        
        mapView.addAnnotations(route)
        
        self.mapView.centerMapOnLocation(location: initialLocation, latitudeDelta: 0.07, longitudeDelta: 0.07)
        //        self.mapView.zoomShowAllPins()
        
        realTimeAnnotation.coordinate = initialLocation.coordinate
        
        mapView.addAnnotation(realTimeAnnotation)
        self.mapView.zoomingPoint(initialLocation.coordinate)
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.mapView.drawDirectionsTo(route: self.arrayMyAnnotationToArrayCLLocationCoordinate2D(self.route))
        }
    }
    
    func arrayMyAnnotationToArrayCLLocationCoordinate2D(_ annotations: [MyAnnotation]) -> [CLLocationCoordinate2D] {
        var route: [CLLocationCoordinate2D] = []
        for pin in annotations{
            route.append(pin.coordinate)
        }
        return route
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var viewReturn: MKAnnotationView
        if let annotation = annotation as? MyAnnotation {
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: annotation.id) {
                dequeuedView.annotation = annotation
                viewReturn = dequeuedView
            } else {
                viewReturn = MKAnnotationView(annotation: annotation, reuseIdentifier: annotation.id)
                viewReturn.image =  annotation.image
            }
        }else{
            viewReturn = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        }
        return viewReturn
    }
    
    func setupRoute() {
        var points: [ CLLocationCoordinate2D] = []
        points.append(CLLocationCoordinate2D(latitude:-22.86070184,longitude:-47.03663617)) //INICIAL
        points.append(CLLocationCoordinate2D(latitude:-22.86078093,longitude:-47.03379571))
        points.append(CLLocationCoordinate2D(latitude:-22.85774339,longitude:-47.03682125))
        points.append(CLLocationCoordinate2D(latitude:-22.85630493,longitude:-47.03762591))
        points.append(CLLocationCoordinate2D(latitude:-22.85442649,longitude:-47.03881145))
        points.append(CLLocationCoordinate2D(latitude:-22.85433998,longitude:-47.03886241))
        points.append(CLLocationCoordinate2D(latitude:-22.85189798,longitude:-47.04128712))
        points.append(CLLocationCoordinate2D(latitude:-22.85110456,longitude:-47.04242706))
        points.append(CLLocationCoordinate2D(latitude:-22.84958199,longitude:-47.04207301))
        points.append(CLLocationCoordinate2D(latitude:-22.8332676,longitude:-47.05182552)) //PUC
        
        route.append(MyAnnotation(title: "", id: "start", image: #imageLiteral(resourceName: "pin_start"), coordinate: points[0]) ) //INICIAL
        route.append(MyAnnotation(title: "", id: "pass1", image: #imageLiteral(resourceName: "pass9"), coordinate: points[1]) )
        route.append(MyAnnotation(title: "", id: "pass2", image: #imageLiteral(resourceName: "pass3"), coordinate: points[2]) )
        route.append(MyAnnotation(title: "", id: "pass3", image: #imageLiteral(resourceName: "pass4"), coordinate: points[3]) )
        route.append(MyAnnotation(title: "", id: "pass4", image: #imageLiteral(resourceName: "pass6"), coordinate: points[4]) )
        route.append(MyAnnotation(title: "", id: "pass5", image: #imageLiteral(resourceName: "pass1"), coordinate: points[5]) )
        route.append(MyAnnotation(title: "", id: "pass6", image: #imageLiteral(resourceName: "pass8"), coordinate: points[6]) )
        route.append(MyAnnotation(title: "", id: "pass7", image: #imageLiteral(resourceName: "pass5"), coordinate: points[7]) )
        route.append(MyAnnotation(title: "", id: "pass8", image: #imageLiteral(resourceName: "pass2"), coordinate: points[8]) )
        route.append(MyAnnotation(title: "", id: "puc", image: #imageLiteral(resourceName: "pin_destino"), coordinate: points[9]) ) //PUC
    }
    
    func setupRouteNavigation() {
        
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86070184,longitude:-47.03663617)) //INICIAL
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86084272,longitude:-47.03602195)) // PASSAGEIRO NOVO
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8610701,longitude:-47.03583956))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86134691,longitude:-47.03553915))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86160395,longitude:-47.03511))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86166326,longitude:-47.03480959))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86174235,longitude:-47.03453064))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86181155,longitude:-47.03417659))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86162866,longitude:-47.03404784))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86118379,longitude:-47.03398347))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86078093,longitude:-47.03379571))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86045222,longitude:-47.03359723))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.86020507,longitude:-47.03373134))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8599678,longitude:-47.03414977))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85970087,longitude:-47.0347935))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85955258,longitude:-47.03555524))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85954764,longitude:-47.03602195))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85928565,longitude:-47.03662813))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85847004,longitude:-47.03666568))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85686845,longitude:-47.03735232))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8553756,longitude:-47.03814626))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85416944,longitude:-47.03910112))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85308191,longitude:-47.04018474))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85253814,longitude:-47.04071045))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85184607,longitude:-47.04167604))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85150004,longitude:-47.04260945))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85105513,longitude:-47.04247534))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85072392,longitude:-47.04253435))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.85000218,longitude:-47.04230368))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84812366,longitude:-47.04161704))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84701137,longitude:-47.04108059))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84543436,longitude:-47.0395571))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84452474,longitude:-47.03868806))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84380791,longitude:-47.03821063))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84314545,longitude:-47.03752935))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84328882,longitude:-47.03703582))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84370903,longitude:-47.03694463))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8440106,longitude:-47.03736305))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8439661,longitude:-47.03800678))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84394138,longitude:-47.03881681))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84390184,longitude:-47.03963757))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84389195,longitude:-47.04048514))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84383757,longitude:-47.04170287))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84378319,longitude:-47.04286695))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84373869,longitude:-47.04411149))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84360027,longitude:-47.04535604))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84333826,longitude:-47.0467025))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84286861,longitude:-47.04871416))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84213694,longitude:-47.0515734))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8419639,longitude:-47.05287695))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84189469,longitude:-47.05411077))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84187492,longitude:-47.05534458))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84195896,longitude:-47.05679297))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84209739,longitude:-47.05772102))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84233468,longitude:-47.05920696))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84245333,longitude:-47.06048906))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84242367,longitude:-47.06099868))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8420084,longitude:-47.06137955))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84172166,longitude:-47.06195891))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84148931,longitude:-47.06227005))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.84032752,longitude:-47.06247926))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8391855,longitude:-47.06268847))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.83753425,longitude:-47.06323028))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.83668884,longitude:-47.06335366))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.83537869,longitude:-47.06227005))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.83433056,longitude:-47.06132054))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.83352469,longitude:-47.06056416))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.83339614,longitude:-47.06002772))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8335593,longitude:-47.05941081))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8339301,longitude:-47.05870271))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.83427124,longitude:-47.05810189))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.83495351,longitude:-47.05679297))
        routeNavigation.append(CLLocationCoordinate2D(latitude:-22.8332676,longitude:-47.05182552)) //PUC
    }
}


