//
//  PossibleStatusPresenter.swift
//  TCCWay-Passageiro
//
//  Created by Vinicius Simionato on 10/13/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol SetPossibleStatusPresenterView: NSObjectProtocol {
    func startLoadingSetPossible()
    func finishLoadingSetPossible()
    func successSetPossible(status: PossibleStatus)
    func errorSetPossible(title: String, message: String)
}

class SetPossibleStatusPresenter {
    
    let lineService: LineService?
    weak private var view: SetPossibleStatusPresenterView?
    
    init(lineService: LineService) {
        self.lineService = lineService
    }
    
    func attachView(view: SetPossibleStatusPresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func setPossibleStatus(passangerId: String, lineId: String, go: Bool, back: Bool, date: String) {
        self.view?.startLoadingSetPossible()
        
        lineService?.setPossibleStatus(passangerId: passangerId, lineId: lineId, go: go, back: back, date: date, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.successSetPossible(status: PossibleStatus(JSONString: response.text!)!)
                    default:
                        self.view?.errorSetPossible(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.errorSetPossible(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}



