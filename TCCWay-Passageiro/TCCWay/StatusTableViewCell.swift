//
//  StatusTableViewCell.swift
//  TCCWay-Passageiro
//
//  Created by Vinicius Simionato on 10/12/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class StatusTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayOfWeekLabel: UILabel!
    @IBOutlet weak var goView: UIView!
    @IBOutlet weak var goLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var backLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func render(status: PossibleStatus) {
        dateLabel.text = status.date
        dayOfWeekLabel.text = status.dayOfWeek
        
        if status.go {
            goView.backgroundColor = UIColor(red: 0, green: 0.560, blue: 0, alpha: 1)
            goLabel.text = "Vou"
        } else {
            goView.backgroundColor = UIColor(red: 0.581, green: 0.067, blue: 0, alpha: 1)
            goLabel.text = "Não vou"
        }
        
        
        if status.back {
            backView.backgroundColor = UIColor(red: 0, green: 0.560, blue: 0, alpha: 1)
             backLabel.text = "Volto"
        } else {
            backView.backgroundColor = UIColor(red: 0.581, green: 0.067, blue: 0, alpha: 1)
              backLabel.text = "Não Volto"
        }
    }
}
