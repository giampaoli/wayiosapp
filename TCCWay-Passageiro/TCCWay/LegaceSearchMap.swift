//
//  SearchViewController.swift
//  TCCWay-Passageiro
//
//  Created by Jessica Batista de Barros Cherque on 05/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import MapKit

class LegaceSearchMap: UIViewController {
    
    @IBOutlet weak var myMapView: MKMapView!
    @IBOutlet weak var textSearch: UITextField!
    
    public var pinPrevious :MKPointAnnotation!
    var pinCurrent :MKPointAnnotation!
    public var funcResult: ((MKPointAnnotation)->())!
    let activityIndicatorMap = UIActivityIndicatorView()
    
    @IBAction func touchOk(_ sender: Any) {
        pinCurrent.coordinate = myMapView.centerCoordinate
        pinCurrent.title = textSearch.text
        funcResult(pinCurrent)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if funcResult == nil{
            self.dismiss(animated: true, completion: nil)
        }
        
        textSearch.text = ""
        pinCurrent = MKPointAnnotation()
        
        if pinPrevious == nil {
            pinCurrent.coordinate = CLLocationCoordinate2DMake(0, 0)
        }else {
            pinCurrent = pinPrevious
            
            if pinCurrent.title != nil {
                textSearch.text = pinCurrent.title
            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        markPoint(coordinate: pinCurrent.coordinate, title: pinCurrent.title)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editDidEndPontoInicial(_ sender: Any) {
        self.pauseInteration()
        myMapView.searchRequestTextMap( textSearch.text!, callbackOfSearch)
    }
    
    func callbackOfSearch(_ coordinate: CLLocationCoordinate2D!) {
        
        if coordinate == nil {
            alertNoResult()
            textSearch.resignFirstResponder()
            
        }else{
            self.myMapView.removeAnnotation(pinCurrent)
            markPoint(coordinate: coordinate!, title: self.textSearch.text!)
        }
        
        self.restartInteration()
    }
    
    func markPoint(coordinate: CLLocationCoordinate2D!, title:String!){
        if title != nil && title != "" {
            let pin = MKPointAnnotation()
            pin.coordinate = coordinate
            pin.title = title
            self.myMapView.addAnnotation(pin)
            
            myMapView.zoomingPoint(coordinate)
        }
    }
    
    func alertNoResult() {
        Alert().showWith( controller: self, title: "Nenhum resultado", message: "")
    }
    
    func pauseInteration(){
        //Ignoring user
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        //Activity Indicator
        activityIndicatorMap.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityIndicatorMap.center = self.view.center
        activityIndicatorMap.hidesWhenStopped = true
        activityIndicatorMap.startAnimating()
        
        self.view.addSubview(activityIndicatorMap)
    }
    
    
    func restartInteration(){
        self.activityIndicatorMap.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
}



