//
//  MKMapView+CenterLocation.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 06/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import MapKit

extension MKMapView {
    func centerMapOnLocation(location: CLLocation, latitudeDelta: CLLocationDegrees, longitudeDelta: CLLocationDegrees ) {
        
         self.region.span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta)
        
        let coordinateRegion = MKCoordinateRegionMake(location.coordinate, self.region.span)
        self.setRegion(coordinateRegion, animated: true)
    }

}
