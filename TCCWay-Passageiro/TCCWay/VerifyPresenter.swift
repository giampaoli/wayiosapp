//
//  LoginPresenter.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 24/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol VerifyPresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(verify: Verify)
    func error(title: String, message: String)
}

class VerifyPresenter {
    let verifyService: VerifyService?
    weak private var view: VerifyPresenterView?
    
    init(verifyService: VerifyService) {
        self.verifyService = verifyService
    }
    
    func attachView(view: VerifyPresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func postVerify(telephone: String) {
        self.view?.startLoading()
        
        verifyService?.postVerify(telephoneNumber: telephone, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(verify: Verify(JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}
