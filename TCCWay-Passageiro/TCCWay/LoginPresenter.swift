//
//  LoginPresenter.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 25/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol LoginPresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(user: User)
    func error(title: String, message: String)
}

class LoginPresenter {
    let loginService: LoginService?
    weak private var view: LoginPresenterView?
    
    init(loginService: LoginService) {
        self.loginService = loginService
    }
    
    func attachView(view: LoginPresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func postLogin(token: String, telephone: String) {
        self.view?.startLoading()
        
        loginService?.postLogin(tokenNumber: token, telephone: telephone, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(user: User(JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}
