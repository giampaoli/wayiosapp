//
//  LineTableViewCell.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 03/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class LineTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lineImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleTimeStart: UILabel!
    
    @IBOutlet weak var titleText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func render(line: Line) {
        titleLabel.text = line.title
        titleTimeStart.text = "Inicar as 19:00"

        Alamofire.request(line.imageUrl).responseImage { response in
            
            debugPrint(response)
            
//            print(response.request)
//            print(response.response)
            debugPrint(response.result)
            
            if let image = response.result.value {
                print("image downloaded: \(image)")
                self.lineImage.image = image
            }
        }
    }
}
