//
//  PossibleStatus.swift
//  TCCWay-Passageiro
//
//  Created by Vinicius Simionato on 10/13/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import ObjectMapper

struct PossibleStatus: Mappable {

    private(set) var back = false
    private(set) var go = false
    private(set) var date = ""
    private(set) var dayOfWeek = ""
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        back <- map["volto"]
        go <- map["vou"]
        date <- map["data"]
        dayOfWeek <- map["dia_da_semana"]
    }
}
