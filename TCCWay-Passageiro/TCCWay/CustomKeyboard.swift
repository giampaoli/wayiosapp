//
//  CustomKeyboard.swift
//  TCCWay-Passageiro
//
//  Created by Vinicius Simionato on 10/10/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

protocol KeyboardDelegate: NSObjectProtocol {
    func switchGoWasTapped(newState: Bool)
    func switchBackWasTapped(newState: Bool)
    func selectedDate(date: String)
}

class CustomKeyboard: UIView {
    
    weak var delegate: KeyboardDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }
    
    func attachView(delegate: KeyboardDelegate) {
        self.delegate = delegate
    }
    
    func initializeSubviews() {
        let xibFileName = "CustomKeyboard"
        let view = Bundle.main.loadNibNamed(xibFileName, owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    @IBAction func goValueChanged(_ sender: UISwitch) {
        delegate?.switchGoWasTapped(newState: sender.isOn)
    }
    
    @IBAction func backValueChanged(_ sender: UISwitch) {
        delegate?.switchBackWasTapped(newState: sender.isOn)
    }
    
    @IBAction func dateValueChanged(_ sender: UIDatePicker) {
        delegate?.selectedDate(date: sender.date.toString())
    }
}
