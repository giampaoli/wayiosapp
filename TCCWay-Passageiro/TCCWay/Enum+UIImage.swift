//
//  Enum+UIImage.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 03/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    enum AssetIdentifier: String {
        case payment = "ic_payment"
        case message = "ic_notification"
        case location = "ic_location"
        
        static let value = [payment, message, location]
    }
    
    convenience init!(assetIdentifier: AssetIdentifier) {
        self.init(named: assetIdentifier.rawValue)
    }
}
