//
//  MKMapView+zoom.swift
//  TCCWay-Passageiro
//
//  Created by Jessica Batista de Barros Cherque on 06/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import MapKit

extension MKMapView{

    func zoomShowAllPins(){
        
        if (self.annotations.count > 0) {
            
            let pins = self.annotations
        
            var maxLat = pins[0].coordinate.latitude;
            var maxLon = pins[0].coordinate.longitude;
            var minLat = pins[0].coordinate.latitude;
            var minLon = pins[0].coordinate.longitude;
        
            for pin in pins{
                maxLat = max(pin.coordinate.latitude, maxLat);
                maxLon = max(pin.coordinate.longitude, maxLon);
                minLat = min(pin.coordinate.latitude, minLat);
                minLon = min(pin.coordinate.longitude, minLon);
            }
            
            let latitude     = (maxLat + minLat) / 2;
            let longitude    = (maxLon + minLon) / 2;
            let latitudeDelta  = (maxLat - minLat + 0.05)*1.12;
            let longitudeDelta = (maxLon - minLon + 0.05)*1.12;
        
            let coordinate = CLLocationCoordinate2DMake(latitude , longitude)
            let span = MKCoordinateSpanMake(latitudeDelta , longitudeDelta)
            let region = MKCoordinateRegionMake(coordinate, span)
            self.setRegion(region, animated: true)
        }
    }
    
    
    func zoomingPoint(_ coordinate:CLLocationCoordinate2D ){
        let span = MKCoordinateSpanMake(0.01, 0.01)
        let region = MKCoordinateRegionMake(coordinate, span)
        setRegion(region, animated: true)
    }
}
