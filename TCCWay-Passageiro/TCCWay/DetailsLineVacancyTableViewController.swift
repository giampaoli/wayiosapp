//
//  DetailsLineVacancyTableViewController.swift
//  TCCWay-Passageiro
//
//  Created by Vinicius Simionato on 10/18/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import AlamofireImage

class DetailsLineVacancyTableViewController: UITableViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var availableLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var routeMap: MKMapView!
    @IBOutlet weak var nameDriverLabel: UILabel!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var entryRequestButton: UIButton!
    @IBOutlet weak var imageVan: UIImageView!
    
    var line = Line()
    public var pinStarting :MyAnnotation!
    var image = UIImage()
    var route = [MyAnnotation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.text = line.title
        self.nameDriverLabel.text = line.driverName
        
        self.imageVan.image = image
        
        setupRoute7()
        setupMap()
        renderDriver()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func entryRequest(_ sender: UIButton) {
        print("Entry Request Tap")
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "entryRequest" ,
            let nextScene = segue.destination as? IngressSolicitationViewController {
            nextScene.line = self.line
            nextScene.delegate = self
            nextScene.pinStarting = self.pinStarting
        }
    }
    
    @IBAction func contactDriver(_ sender: UIButton) {
        let contact = UIAlertController(title: "(11) 97108-6251", message: nil, preferredStyle: .actionSheet)
        
        contact.addAction(UIAlertAction(title: "Ligar", style: .default, handler: callHandler))
        
        self.present(contact, animated: true, completion: nil)
    }
    
    private func callHandler(selectedOption: UIAlertAction) {
        let url: NSURL = URL(string: "TEL://11971086251")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    func renderDriver() {
        imageContainerView.cornerRadius = Double(imageContainerView.frame.width / 2)
        imageContainerView.clipsToBounds = true
        mainImageView.cornerRadius = Double(mainImageView.frame.width / 2)
        mainImageView.clipsBounds = true
        
        imageContainerView.borderWidth = Double((1.5 / 29.5) * mainImageView.frame.size.width)
        imageContainerView.borderColor = UIColor.white
        
        //TODO: Desmocar
        Alamofire.request("https://randomuser.me/api/portraits/men/88.jpg").responseImage { response in
            debugPrint(response)
            if let image = response.result.value {
                print("image downloaded: \(image)")
                self.mainImageView.image = image
            }
        }
    }
}

extension DetailsLineVacancyTableViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 3
        return renderer
    }
    
    func setupMap() {
//        routeMap.delegate = self
        
        let initialLocation = CLLocation(latitude: (route.first!.coordinate.latitude), longitude: (route.first!.coordinate.longitude))
        
        routeMap.addAnnotations(self.route)
        
        self.routeMap.centerMapOnLocation(location: initialLocation, latitudeDelta: 0.07, longitudeDelta: 0.07)
        self.routeMap.zoomShowAllPins()
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.routeMap.drawDirectionsTo(route: self.arrayMyAnnotationToArrayCLLocationCoordinate2D(self.route))
        }
    }
    
    func arrayMyAnnotationToArrayCLLocationCoordinate2D(_ annotations: [MyAnnotation]) -> [CLLocationCoordinate2D] {
        var route: [CLLocationCoordinate2D] = []
        for pin in annotations{
            route.append(pin.coordinate)
        }
        return route
    }

    func setupRoute7() {
        var points: [ CLLocationCoordinate2D] = []
        points.append(CLLocationCoordinate2D(latitude:-22.86070184,longitude:-47.03663617)) //INICIAL
        points.append(CLLocationCoordinate2D(latitude:-22.86078093,longitude:-47.03379571))
        points.append(CLLocationCoordinate2D(latitude:-22.85774339,longitude:-47.03682125))
        points.append(CLLocationCoordinate2D(latitude:-22.85630493,longitude:-47.03762591))
        points.append(CLLocationCoordinate2D(latitude:-22.85442649,longitude:-47.03881145))
        points.append(CLLocationCoordinate2D(latitude:-22.85433998,longitude:-47.03886241))
        points.append(CLLocationCoordinate2D(latitude:-22.85189798,longitude:-47.04128712))
        points.append(CLLocationCoordinate2D(latitude:-22.85110456,longitude:-47.04242706))
        points.append(CLLocationCoordinate2D(latitude:-22.84958199,longitude:-47.04207301))
        points.append(CLLocationCoordinate2D(latitude:-22.8332676,longitude:-47.05182552)) //PUC
        
        route.append(MyAnnotation(title: "", id: "start", image: #imageLiteral(resourceName: "pin_start"), coordinate: points[0]) ) //INICIAL
        //        route.append(MyAnnotation(title: "", id: "pass1", image: #imageLiteral(resourceName: "pass9"), coordinate: points[1]) )
        route.append(MyAnnotation(title: "", id: "pass", image: #imageLiteral(resourceName: "pinpass"), coordinate: points[2]) )
        route.append(MyAnnotation(title: "", id: "pass", image: #imageLiteral(resourceName: "pinpass"), coordinate: points[3]) )
        route.append(MyAnnotation(title: "", id: "pass", image: #imageLiteral(resourceName: "pinpass"), coordinate: points[4]) )
        route.append(MyAnnotation(title: "", id: "pass", image: #imageLiteral(resourceName: "pinpass"), coordinate: points[5]) )
        route.append(MyAnnotation(title: "", id: "pass", image: #imageLiteral(resourceName: "pinpass"), coordinate: points[6]) )
        route.append(MyAnnotation(title: "", id: "pass", image: #imageLiteral(resourceName: "pinpass"), coordinate: points[7]) )
        route.append(MyAnnotation(title: "", id: "pass", image: #imageLiteral(resourceName: "pinpass"), coordinate: points[8]) )
        route.append(MyAnnotation(title: "", id: "puc", image: #imageLiteral(resourceName: "pin_destino"), coordinate: points[9]) ) //PUC
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var viewReturn: MKAnnotationView
        if let annotation = annotation as? MyAnnotation {
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: annotation.id) {
                dequeuedView.annotation = annotation
                viewReturn = dequeuedView
            } else {
                viewReturn = MKAnnotationView(annotation: annotation, reuseIdentifier: annotation.id)
                viewReturn.image =  annotation.image
            }
        }else{
            viewReturn = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        }
        return viewReturn
    }
    
}

extension DetailsLineVacancyTableViewController: IngressSolicitationViewControllerrDelegate {
    func successEntryRequest() {
        entryRequestButton.backgroundColor = UIColor.orange
        entryRequestButton.setTitle("Aguardando Aprovação", for: .normal)
    }


}


