//
//  IngressSolicitationViewController.swift
//  TCCWay-Passageiro
//
//  Created by Jessica Batista de Barros Cherque on 09/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SwiftyBeaver


protocol IngressSolicitationViewControllerrDelegate {
    func successEntryRequest()
}

class IngressSolicitationViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var textAddress: UILabel!
    
    public var pinStarting :MyAnnotation!
    let log = SwiftyBeaver.self
    fileprivate let presenter = EntryRequestPresenter(entryRequestService: EntryRequestService())
    
    var coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    var delegate: IngressSolicitationViewControllerrDelegate?
    var line = Line()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attachView(view: self)
        
        if( pinStarting != nil ){
            coordinate = pinStarting.coordinate
        }
        mapView.delegate = self
        geoCode(location: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
        mapView.zoomingPoint(coordinate)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func geoCode(location : CLLocation!){
        /* Only one reverse geocoding can be in progress at a time hence we need to cancel existing
         one if we are getting location updates */
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (data, error) -> Void in
            guard let placeMarks = data as [CLPlacemark]! else {
                return
            }
            let loc: CLPlacemark = placeMarks[0]
            let addressDict : [NSString:NSObject] = loc.addressDictionary as! [NSString: NSObject]
            let addrList = addressDict["FormattedAddressLines"] as! [String]
            let address = addrList.joined(separator: ", ")
            print(address)
            self.textAddress.text = address
            //self.previousAddress = address
        })
        
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        coordinate = mapView.centerCoordinate
        geoCode(location: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
    }
    
    func setupPonto(_ pin: MKPointAnnotation){
        self.textAddress.text = pin.subtitle
        mapView.zoomingPoint(pin.coordinate)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = segue.destination as! SearchLocationViewController
        dest.funcResult = self.setupPonto
        dest.navigationItem.title = "Ponto de encontro"
    }
    
    @IBAction func clickEntryRequest(_ sender: UIBarButtonItem) {
        presenter.setEntryRequests(lineId: line.idLine.description, passangerId: User.user().id.description, point: Point(latitude: coordinate.latitude, longitude: coordinate.longitude))
    }
}

extension IngressSolicitationViewController: EntryRequestPresenterPresenterView {
    func startLoading() {
        log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func success(entryRequest: EntryRequest) {
        log.info(entryRequest)
        presentSucess()
    }
    
    func error(title: String, message: String) {
        log.error(message)
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
    
    private func presentSucess() {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: "Solicitado com Sucesso!",
            message: "O motorista foi notificado sobre sua solicitação, aguarde a avaliação.",
            okFunction: callbackOk
        )
    }
    
    private func callbackOk() {
        navigationController?.popViewController(animated: true)
        delegate?.successEntryRequest()
    }
}
