//
//  LinePresenter.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 29/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol SearchLinePresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(lines: [Line])
    func error(title: String, message: String)
}

class SearchLinePresenter {
    
    let searchLineService: SearchLineService?
    weak private var view: SearchLinePresenterView?
    
    init(searchLineService: SearchLineService) {
        self.searchLineService = searchLineService
    }
    
    func attachView(view: SearchLinePresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func getLines(startPoint: Point, endPoint: Point) {
        self.view?.startLoading()
        
        searchLineService?.getLines(startPoint: startPoint, endPoint: endPoint, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(lines: [Line](JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}

