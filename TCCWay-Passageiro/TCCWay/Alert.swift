
import UIKit

struct Alert {
    
    func showWith(controller: UIViewController,
                  title: String,
                  message: String,
                  okFunction: (() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction: UIAlertAction!
        if let okFunction = okFunction {
            okAction = UIAlertAction(title: "OK", style: .default, handler: { _ -> Void in
                okFunction()
            })
        } else {
            okAction = UIAlertAction(title: "OK", style: .cancel, handler: { _ -> Void in
//                controller.dismiss(animated: true, completion: nil)
            })
        }
        alertController.addAction(okAction)
        
        DispatchQueue.main.async {
            controller.present(alertController, animated: true, completion: nil)
        }
    }
}
