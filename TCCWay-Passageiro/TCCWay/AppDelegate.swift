//
//  AppDelegate.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 24/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import SwiftyBeaver

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let log = SwiftyBeaver.self

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupSessionUser()

        let console = ConsoleDestination()
        log.addDestination(console)
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    func setupSessionUser() {
        window?.rootViewController = UIStoryboard(name: User.isLogged() ? "Home" : "Login", bundle: Bundle.main).instantiateInitialViewController()
    }
}

