//
//  EntryRequestService.swift
//  TCCWay-Passageiro
//
//  Created by Vinicius Simionato on 10/23/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

class EntryRequestService {
    func entryRequest(lineId: String, passangerId: String, point: Point, callback: @escaping (Response) -> Void) {
        print("SERVICE - entryRequest")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.entry_request
        let parameters = ["id_passageiro": passangerId, "id_linha": lineId, "latitude": point.latitude, "longitude": point.longitude] as [String : Any]
    
        print(urlString)
        print(parameters)
        
        do {
            let opt = try HTTP.POST(urlString, parameters: parameters, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
}


