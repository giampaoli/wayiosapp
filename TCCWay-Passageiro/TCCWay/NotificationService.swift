//
//  NotificationService.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 07/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

class NotificationService {
    func getNotifications(lineId: String, callback: @escaping (Response) -> Void) {
        print("SERVICE - getNotifications")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.notifications_by_line + lineId;
        
        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
}
