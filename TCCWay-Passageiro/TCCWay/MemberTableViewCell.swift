//
//  MemberTableViewCell.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 08/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class MemberTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addBorderColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.backgroundColor = .clear
        
        imageContainerView.cornerRadius = Double(imageContainerView.frame.width / 2)
        imageContainerView.clipsToBounds = true
        mainImageView.cornerRadius = Double(mainImageView.frame.width / 2)
        mainImageView.clipsBounds = true
    }
    
    func render(user: User){
        self.selectionStyle = .none
        
        labelName.text = user.name
        
        Alamofire.request(user.url_imagem).responseImage { response in
            debugPrint(response)
            if let image = response.result.value {
                print("image downloaded: \(image)")
                self.mainImageView.image = image
            }
        }
    }
    
    func addBorderColor() {
        imageContainerView.borderWidth = Double((1.5 / 29.5) * mainImageView.frame.size.width)
        imageContainerView.borderColor = UIColor.white
    }

}
