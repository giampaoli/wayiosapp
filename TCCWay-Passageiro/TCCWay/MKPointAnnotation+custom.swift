//
//  Annotation+custom.swift
//  TCCWay-Passageiro
//
//  Created by Jessica Batista de Barros Cherque on 06/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import MapKit

extension MKPointAnnotation {
    
    func pinStartLine( coordinate:CLLocationCoordinate2D){
        self.coordinate = coordinate
    }

    
    func pinDestinyLine( coordinate:CLLocationCoordinate2D){
        self.coordinate = coordinate
    }
    
    
    func pinPassengerLine( coordinate:CLLocationCoordinate2D){
        self.coordinate = coordinate
    }
    
    func pinOfAddMyPointLine( coordinate:CLLocationCoordinate2D){
        self.coordinate = coordinate
    }

    
}
