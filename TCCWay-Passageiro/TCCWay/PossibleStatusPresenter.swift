//
//  PossibleStatusPresenter.swift
//  TCCWay-Passageiro
//
//  Created by Vinicius Simionato on 10/13/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol PossibleStatusPresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(status: [PossibleStatus])
    func error(title: String, message: String)
}

class PossibleStatusPresenter {
    
    let lineService: LineService?
    weak private var view: PossibleStatusPresenterView?
    
    init(lineService: LineService) {
        self.lineService = lineService
    }
    
    func attachView(view: PossibleStatusPresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func getPossibleStatus(passangerId: String, lineId: String) {
        self.view?.startLoading()
        
        lineService?.getPossibleStatus(passangerId: passangerId, lineId: lineId, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(status: [PossibleStatus](JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}


