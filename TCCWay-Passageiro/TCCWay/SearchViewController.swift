//
//  SearchViewController.swift
//  TCCWay-Passageiro
//
//  Created by Jessica Batista de Barros Cherque on 05/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import MapKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var buttonPointStart: UIButton!
    @IBOutlet weak var buttonPointDestiny: UIButton!
    @IBOutlet weak var textPointStart: UILabel!
    @IBOutlet weak var textPointDestiny: UILabel!
    var pinStarting :MyAnnotation!
    var pinDestiny :MyAnnotation!
    @IBOutlet weak var buttonSearch: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonSearch.isEnabled = false
        buttonSearch.backgroundColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        
        self.navigationItem.title = "Busca"
        
        let span = MKCoordinateSpanMake(0.5, 0.5)
        let location = CLLocationCoordinate2D(latitude: -22.91128211, longitude: -47.06886292)
        let region = MKCoordinateRegionMake((location, span))
        mapView.setRegion(region, animated: true)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupPinDestiny(_ pin: MKPointAnnotation){
        self.pinDestiny = MyAnnotation(title: pin.title!, id: "destiny",  image: #imageLiteral(resourceName: "pin_destino"),  coordinate: pin.coordinate)
        self.textPointDestiny.text = pin.title
        reloadDataMap()
    }
    
    func setupPinStarting(_ pin: MKPointAnnotation){
        self.pinStarting = MyAnnotation(title: pin.title!, id: "start", image: #imageLiteral(resourceName: "pin_start"),  coordinate: pin.coordinate)
        self.textPointStart.text = pin.title
        reloadDataMap()
    }
    
    func reloadDataMap(){
        if pinStarting != nil || pinDestiny != nil {
            let annotations = mapView.annotations
            mapView.removeAnnotations(annotations)
        }
        
        if pinStarting != nil{
            mapView.addAnnotation(pinStarting)
        }
        
        if pinDestiny != nil{
            mapView.addAnnotation(pinDestiny)
        }
        
        if pinStarting != nil && pinDestiny != nil {
            mapView.zoomShowAllPins()
            buttonSearch.isEnabled = true
            buttonSearch.backgroundColor = #colorLiteral(red: 0.8997858503, green: 0.5267967796, blue: 0.006553453362, alpha: 0.9444028253)
        }else{
            if pinStarting != nil{
                mapView.zoomingPoint(pinStarting.coordinate)
            }
            
            if pinDestiny != nil{
                mapView.zoomingPoint(pinDestiny.coordinate)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let segueID = segue.identifier {
            switch segueID {
            case "segueStarting":
                //Pesquisa no mapa ponto de inicio
                let dest = segue.destination as! SearchLocationViewController
                dest.funcResult = self.setupPinStarting
                dest.navigationItem.title = "Ponto Inicial"
            case "segueDestiny":
                //Pesquisa no mapa ponto de destino
                let dest = segue.destination as! SearchLocationViewController
                dest.funcResult = self.setupPinDestiny
                dest.navigationItem.title = "Destino/ Universidade"
            case "segueSearchContinue":
                let dest = segue.destination as! ResultSearchTableViewController
                dest.pinStarting = self.pinStarting
                dest.pinDestiny = self.pinDestiny
                
            //Busca no servidor linhas de acordo com os pontos
            default:
                break
                //Do nothing
            }
        }
    }
}

extension SearchViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var viewReturn: MKAnnotationView
        
        if let annotation = annotation as? MyAnnotation {
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: annotation.id) {
                dequeuedView.annotation = annotation
                viewReturn = dequeuedView
            } else {
                viewReturn = MKAnnotationView(annotation: annotation, reuseIdentifier: annotation.id)
                viewReturn.image =  annotation.image
            }
        }else{
            viewReturn = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        }
        return viewReturn
    }
    
    
}



