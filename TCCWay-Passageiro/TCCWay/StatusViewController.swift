//
//  StatusViewController.swift
//  TCCWay-Passageiro
//
//  Created by Vinicius Simionato on 10/10/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import SwiftyBeaver

class StatusViewController: UIViewController {
    
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let log = SwiftyBeaver.self
    fileprivate let presenter = PossibleStatusPresenter(lineService: LineService())
    fileprivate let presenterSetStatus = SetPossibleStatusPresenter(lineService: LineService())
    
    var date = "06/12/2017"
    var lineId = ""
    var goState =  true
    var backState =  true
    var status : [PossibleStatus]? = nil
    var keyboardView = CustomKeyboard(frame: CGRect(x: 0, y: 0, width: 0, height: 330))
    
    @IBOutlet weak var addBarButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBarButton.target = self
        addBarButton.action = #selector(StatusViewController.clickAdd)
        
        keyboardView.attachView(delegate: self)
        presenter.attachView(view: self)
        presenterSetStatus.attachView(view: self)
        
        presenter.getPossibleStatus(passangerId: User.user().id.description, lineId: lineId)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func clickAdd() {
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let margin:CGFloat = 10.0
        let rect = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: 330)
        
        keyboardView.frame = rect
        alertController.view.addSubview(keyboardView)
        
        let somethingAction = UIAlertAction(title: "Inserir Status Eventual", style: .default, handler: {(alert: UIAlertAction!) in
            print("something")
            self.clickSetStatus()
        })
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
        
        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion:{})
    }
    
    func clickSetStatus() {
        presenterSetStatus.setPossibleStatus(passangerId: User.user().id.description, lineId: lineId, go: goState, back: backState, date: date)
    }
}

extension StatusViewController : KeyboardDelegate {
    func switchGoWasTapped(newState: Bool) {
        print(newState)
        self.goState = newState
    }
    
    func switchBackWasTapped(newState: Bool) {
        print(newState)
        self.backState = newState
    }
    
    func selectedDate(date: String) {
        print(date)
        self.date = date
    }
}

extension StatusViewController :  UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return status != nil ? status!.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "statusCell", for: indexPath) as? StatusTableViewCell
        
        cell?.render(status: status![indexPath.row])
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            status?.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
}

extension StatusViewController : PossibleStatusPresenterView {
    func startLoading() {
        log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func success(status: [PossibleStatus]) {
        log.info(status)
        
        self.status = status
        self.tableView.reloadData()
    }
    
    func error(title: String, message: String) {
        log.error(message)
        presentError(title: title, message: message)
    }
    
    func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
}

extension StatusViewController : SetPossibleStatusPresenterView {
    func startLoadingSetPossible() {
        log.verbose("Start Loading")
    }
    
    func finishLoadingSetPossible() {
        log.verbose("Finish Loading")
    }
    
    func successSetPossible(status: PossibleStatus) {
        log.info(status)
        presenter.getPossibleStatus(passangerId:  User.user().id.description, lineId: lineId)
    }
    
    func errorSetPossible(title: String, message: String) {
        log.error(message)
        presentError(title: title, message: message)
    }
}
