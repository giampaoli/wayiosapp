//
//  ResultSearchTableViewController.swift
//  TCCWay-Passageiro
//
//  Created by Jessica Batista de Barros Cherque on 08/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import MapKit
import SwiftyBeaver

class ResultSearchTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textPointStart: UILabel!
    @IBOutlet weak var textPointDestiny: UILabel!
    
    
    public var pinStarting :MyAnnotation!
    public var pinDestiny :MyAnnotation!
    
    var lines: [Line]!
    
    let log = SwiftyBeaver.self
    fileprivate let presenter = SearchLinePresenter(searchLineService: SearchLineService())
    
    var filter = UIBarButtonItem()
    var bel = UIBarButtonItem()
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        
        filter = UIBarButtonItem(image: #imageLiteral(resourceName: "filtrar"), style: .plain, target: self, action: #selector(filterTouch))
        
        bel = UIBarButtonItem(image: #imageLiteral(resourceName: "bel"), style: .plain, target: self, action: #selector(belTouch))
        
        navigationItem.rightBarButtonItems = [filter, bel]
        
        
        let start = Point(latitude: 0, longitude: 0)
        let end =  Point(latitude: 0, longitude: 0)
        presenter.getLines(startPoint: start, endPoint: end)
    }
    
    func belTouch(_ sender: Any) {
        presentMessage()
        
        let image = bel.image
        if image == #imageLiteral(resourceName: "bel") {
            bel.image = #imageLiteral(resourceName: "belSelected")
        }else{
            bel.image = #imageLiteral(resourceName: "bel")
        }
    }
    
    func filterTouch(_ sender: Any) {
        self.performSegue(withIdentifier: "GoToFilter", sender: sender)
    }
    
    private func presentMessage() {
        let message = Alert()
        
        message.showWith(
            controller: self,
            title: "Notificações Ativadas",
            message: "Sempre que uma nova vaga para linhas com esse destino for adicionada voce será notificado! :D"
        )
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //        self.textPointStart.text = self.pinStarting.title
        //        self.textPointDestiny.text = self.pinDestiny.title
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setupPinDestiny(_ pin: MKPointAnnotation){
        self.pinDestiny = MyAnnotation(title: pin.title!, id: "destiny", image: #imageLiteral(resourceName: "calendario"),  coordinate: pin.coordinate)
    }
    
    func setupPinStarting(_ pin: MKPointAnnotation){
        self.pinStarting = MyAnnotation(title: pin.title!, id:"start",  image: #imageLiteral(resourceName: "calendario"),  coordinate: pin.coordinate)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segueID = segue.identifier {
            switch segueID {
            case "segueStarting":
                //Pesquisa no mapa ponto de inicio
                let dest = segue.destination as! SearchLocationViewController
                dest.funcResult = self.setupPinStarting
                dest.navigationItem.title = "Ponto Inicial"
            case "segueDestiny":
                //Pesquisa no mapa ponto de destino
                let dest = segue.destination as! SearchLocationViewController
                dest.funcResult = self.setupPinDestiny
                dest.navigationItem.title = "Destino/ Universidade"
            case "lineDetails":
                if segue.identifier == "lineDetails" ,
                    let nextScene = segue.destination as? DetailsLineVacancyTableViewController ,
                    let indexPath = self.tableView.indexPathForSelectedRow {
                    
                    let cell = self.tableView.cellForRow(at: indexPath) as? SearchLineTableViewCell
                    
                    nextScene.image = (cell?.lineImage.image)!
                    nextScene.line = (lines?[indexPath.row])!
                    nextScene.pinStarting = self.pinStarting
                }
            default:
                break
            }
        }
    }
}

extension ResultSearchTableViewController: SearchLinePresenterView {
    func startLoading() {
        log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func success(lines: [Line]) {
        log.info(lines)
        self.lines = lines
        self.tableView.reloadData()
    }
    
    func error(title: String, message: String) {
        log.error(message)
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
}

extension ResultSearchTableViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lines != nil ? lines.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "line", for: indexPath) as? SearchLineTableViewCell
        
        cell?.render(line: (lines?[indexPath.row])!)
        
        return cell!
    }
}

extension ResultSearchTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "lineDetails", sender: self)
    }
}
