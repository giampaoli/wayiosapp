//
//  TokenTextFieldFormatRules.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 24/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class TokenTextFieldFormatRules: FormattedTextFieldRules {
    
    override func genericCharacter() -> String {
        return "0"
    }
    
    override func desiredFormat(string: String) -> String {
        return "0   0   0   0   0   0"
    }
    
    override func isFinished(textField: UITextField) -> Bool {
        let unformattedCurrentText = self.unformatted(string: textField.text ?? "")
        return unformattedCurrentText.length == 6
    }
    
    override func isValid(textField: UITextField) -> Bool {
        let unformattedCurrentText = self.unformatted(string: textField.text ?? "")
        return super.isValid(textField:textField) && unformattedCurrentText != ""
    }
    
}
