//
//  ProfileViewController.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 27/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        drawProfileImage()
        
        nameLabel.text = User.user().name
        //        numberLabel.text = User.user().phone.description
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func drawProfileImage() {
        imageContainerView.cornerRadius = Double(imageContainerView.frame.width / 2)
        imageContainerView.clipsToBounds = true
        mainImageView.cornerRadius = Double(mainImageView.frame.width / 2)
        mainImageView.clipsBounds = true
        
        Alamofire.request(User.user().url_imagem).responseImage { response in
            debugPrint(response)
            if let image = response.result.value {
                print("image downloaded: \(image)")
                self.mainImageView.image = image
            }
        }
    }
    
    @IBAction func touchLogout(_ sender: Any) {
        UIView.transition(with: (self.view?.window)!, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {() -> Void in
            
            self.view?.window?.rootViewController = UIStoryboard(name: "Login", bundle: Bundle.main).instantiateInitialViewController()
            
        }, completion: nil)
        
        User.logout()
    }
}
