//
//  TokenTextField.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 24/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class TokenTextField: FormattedTextField {
    
    override func initialize() {
        super.initialize()
        self.formatRules = TokenTextFieldFormatRules()
    }
    
    override func text(in range: UITextRange) -> String? {
        return text?.replacingOccurrences(of: " ", with: "")
    }
}
