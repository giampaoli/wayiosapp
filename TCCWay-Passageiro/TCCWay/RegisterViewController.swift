//
//  RegisterViewController.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 25/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Registre-se";
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
