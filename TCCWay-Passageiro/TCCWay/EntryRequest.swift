//
//  InputRequest.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 9/16/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import ObjectMapper
import MapKit

struct EntryRequest: Mappable {
    
    private(set) var name = ""
    private(set) var urlPictureUser = ""
    private(set) var phoneNumber = ""
    private(set) var latitude: Double = 0
    private(set) var longitude: Double = 0
    private(set) var location: CLLocationCoordinate2D? = nil
    
    init?(map: Map) {}
    init?() {}
    
    mutating func mapping(map: Map) {
        name <- map["nome"]
        urlPictureUser <- map["url_imagem"]
        phoneNumber <- map["telefone"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        
        if latitude != 0 && longitude != 0 {
            location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
    }
}
