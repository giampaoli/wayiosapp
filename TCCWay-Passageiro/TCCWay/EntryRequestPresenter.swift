//
//  InputRequestPresenter.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 9/16/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol EntryRequestPresenterPresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(entryRequest: EntryRequest)
    func error(title: String, message: String)
}

class EntryRequestPresenter {
    
    let entryRequestService: EntryRequestService?
    weak private var view: EntryRequestPresenterPresenterView?
    
    init(entryRequestService: EntryRequestService) {
        self.entryRequestService = entryRequestService
    }
    
    func attachView(view: EntryRequestPresenterPresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func setEntryRequests(lineId: String, passangerId: String, point: Point) {
        self.view?.startLoading()
        
        entryRequestService?.entryRequest(lineId: lineId, passangerId: passangerId, point: point, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(entryRequest: EntryRequest(JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}
