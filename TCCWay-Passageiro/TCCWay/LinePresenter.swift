//
//  LinePresenter.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 29/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol LinePresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(lines: [Line])
    func error(title: String, message: String)
}

class LinePresenter {
    
    let lineService: LineService?
    weak private var view: LinePresenterView?
    
    init(lineService: LineService) {
        self.lineService = lineService
    }
    
    func attachView(view: LinePresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func getLines(driverId: String) {
        self.view?.startLoading()
        
        lineService?.getLines(driverId: driverId, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(lines: [Line](JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}
