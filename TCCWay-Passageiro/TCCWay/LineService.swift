//
//  LineService.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 29/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

class LineService {
    func getLines(driverId: String, callback: @escaping (Response) -> Void) {
        print("SERVICE - getLines")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.lines_by_passenger + driverId
        
        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
    
    func getInputRequests(lineId: String, callback: @escaping (Response) -> Void) {
        print("SERVICE - getLines")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.input_request_line + lineId
        
        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
    
    func getMembers(lineId: String, callback: @escaping (Response) -> Void) {
        print("SERVICE - getMembers")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.passenger_by_line + lineId
        
        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
    
    func getPossibleStatus(passangerId: String, lineId: String, callback: @escaping (Response) -> Void) {
        print("SERVICE - getPossibleStatus")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.list_possible_status_passenger + passangerId + "/" + lineId
        
        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
    
    func setPossibleStatus(passangerId: String, lineId: String, go: Bool,  back: Bool, date: String , callback: @escaping (Response) -> Void) {
        print("SERVICE - setPossibleStatus")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.add_possible_status_passenger
        let parameters = ["id_passageiro": passangerId, "id_linha": lineId, "data": date, "vou": go ? 1 : 0, "volto": back ? 1 : 0] as [String : Any]
        
        print(urlString)
        print(parameters)
        
        do {
            let opt = try HTTP.POST(urlString, parameters: parameters, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
}
