//
//  SearchTabViewController.swift
//  TCCWay-Passageiro
//
//  Created by Jessica Batista de Barros Cherque on 03/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class SearchTabViewController: TabsViewController {
    
    override func viewDidLoad() {
        storyboardName = "Search"
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
