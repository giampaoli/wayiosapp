//
//  Button+Loading.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 25/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//
import Foundation
import UIKit

extension UIButton {
    
    func loadingIndicator(show: Bool) {
        let tag = 9876
        
        if show {
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            indicator.color = UIColor.white
            setTitleColor(.clear, for: .normal)
            
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
                setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
}
