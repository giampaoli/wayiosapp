//
//  MKMapView+DrawDirections.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 06/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation

import MapKit

extension MKMapView {
    func drawDirectionsTo(route : [CLLocationCoordinate2D]) {
        
        var pointA:CLLocationCoordinate2D? = nil
        var pointB:CLLocationCoordinate2D? = nil
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var overlays = [MKOverlay]()
        
        for coordinate in route {
            
            if pointA == nil {
                pointA = coordinate
                continue
            }
            
            pointB = coordinate
            
            let request = MKDirectionsRequest()
            request.source = MKMapItem(placemark: MKPlacemark(coordinate: pointA!))
            request.destination = MKMapItem(placemark: MKPlacemark(coordinate: pointB!))
            request.requestsAlternateRoutes = false
            request.transportType = .automobile
            
            let directions = MKDirections(request: request)
            directions.calculate { (response, error) in
                if error == nil {
                    if let routes = response?.routes {
                        for route in routes {
                            print(route.distance)
                            overlays.append(route.polyline)
                        }
                    }
                }
                semaphore.signal()
            }
            
            _ = semaphore.wait(wallTimeout: .distantFuture)
            
            pointA = pointB
        }
        
        DispatchQueue.main.async {
            self.addOverlays(overlays)
        }
    }
}
