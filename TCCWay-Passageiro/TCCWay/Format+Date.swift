//
//  Format+Date.swift
//  TCCWay-Passageiro
//
//  Created by Vinicius Simionato on 10/12/17.
//  Copyright © 2017 TCC. All rights reserved.
//
import Foundation
import UIKit

extension Date {
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: self)
    }
}
