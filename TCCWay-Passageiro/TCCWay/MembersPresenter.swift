//
//  MembersPresenter.swift
//  TCCWay-Passageiro
//
//  Created by Vinicius Simionato on 10/7/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol MemberPresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(users: [User])
    func error(title: String, message: String)
}

class MembersPresenter {
    
    let lineService: LineService?
    weak private var view: MemberPresenterView?
    
    init(lineService: LineService) {
        self.lineService = lineService
    }
    
    func attachView(view: MemberPresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func getMembers(lineId: String) {
        self.view?.startLoading()
        
        lineService?.getMembers(lineId: lineId, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(users: [User](JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}

