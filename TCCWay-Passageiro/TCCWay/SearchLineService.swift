//
//  LineService.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 29/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

class SearchLineService {
    func getLines(startPoint: Point, endPoint: Point, callback: @escaping (Response) -> Void) {
        print("SERVICE - getLines")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.all_lines

        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
}

