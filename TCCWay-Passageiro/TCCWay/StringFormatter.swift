import Foundation

struct StringFormatter {
    
    static func formatted(string: String, format: String, genericCharacter: String) -> String {
        var resultString = ""
        var inputText = string
        
        for i in (0..<format.characters.count) {
            if inputText == "" {
                break
            }
            
            if format[i] == genericCharacter[0] {
                resultString += inputText[0]
                inputText = inputText[1...inputText.characters.count]
            } else {
                resultString += format[i]
            }
        }
        
        return resultString
    }
    
    // format must not contain characters that user can type
    static func unformatted(string: String, format: String, genericCharacter: String) -> String {

    let pattern = format.replacingOccurrences(of: genericCharacter, with: "")
        var resultString = ""
        
        for i in (0..<string.characters.count) {
            if pattern.range(of: string[i]) == nil {
                resultString += string[i]
            }
        }
        
        return resultString
    }
    
}
