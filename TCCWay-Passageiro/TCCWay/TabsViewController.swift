//
//  TabsViewController.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 29/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class TabsViewController:UIViewController {
    
    var storyboardName:String?
    
    override func viewDidLoad() {
        let storyboard = UIStoryboard(name: storyboardName!, bundle: nil)
        let controller = storyboard.instantiateInitialViewController()
        addChildViewController(controller!)
        view.addSubview(controller!.view)
        controller?.didMove(toParentViewController: self)
        
        edgesForExtendedLayout = UIRectEdge.bottom
        extendedLayoutIncludesOpaqueBars = true
    }
}
