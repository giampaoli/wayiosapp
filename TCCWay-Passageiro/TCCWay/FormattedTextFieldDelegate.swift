//
//  FormattedTextFieldDelegate.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 24/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//
protocol FormattedTextFieldDelegate: class {
    func textFieldDidFinshEditing(textField: FormattedTextField)
}
