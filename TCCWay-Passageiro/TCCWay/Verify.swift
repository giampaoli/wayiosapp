//
//  Login.swift
//  PlusWay
//
//  Created by Vinicius Simionato on 24/08/17.
//  Copyright © 2017 plusway. All rights reserved.
//

import ObjectMapper

struct Verify: Mappable {
    
    private(set) var telephone = ""
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        telephone <- map["telefone"]
        
    }
}
