//
//  Contacts
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 04/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import Contacts
import ObjectMapper

class Contact: Equatable {
    var name: String?
    var photo: UIImage?
    var number: String?
    
    final class func == (lhs: Contact, rhs: Contact) -> Bool {
        return lhs.number == rhs.number
    }
}

class Contacts {
    
    static let sharedInstance = Contacts()
    
    private init() {}
    
    lazy var contacts: [CNContact]! = {
        let contactStore = CNContactStore()
        let keysToFetch: [CNKeyDescriptor] = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                                              CNContactEmailAddressesKey as CNKeyDescriptor,
                                              CNContactPhoneNumbersKey as CNKeyDescriptor,
                                              CNContactImageDataKey as CNKeyDescriptor,
                                              CNContactImageDataAvailableKey as CNKeyDescriptor,
                                              CNContactThumbnailImageDataKey as CNKeyDescriptor]
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch)
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        
        return results
    }()
    
    func getContacts() -> [Contact] {
        
        var contacts = [Contact]()
        
        for c in self.contacts {
            
            var newContact = Contact()
            
            var name = c.givenName
            name = name.appending(c.familyName != "" ? " \(c.familyName)" : "")
            newContact.name = name
            
            let phoneNumber = c.phoneNumbers[0]
            let phoneNumberStruct = phoneNumber.value
            let phoneNumberString = phoneNumberStruct.stringValue
            let phoneNumberToCompare = phoneNumberString.onlyDigits()
            newContact.number = phoneNumberToCompare
            
            let imageName: String = "ic_contact"
            let insets: UIEdgeInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
            let photo: UIImage = (UIImage(named: imageName)?.imageWithInsets(insets: insets))!
            newContact.photo = photo

            contacts.append(newContact)
        }
    
        return contacts
    }
}

extension String {
    func capitalizeFirst() -> String {
        guard self  != "iPhone" && self != "" else {
            return self
        }
        let firstIndex = self.index(startIndex, offsetBy: 1)
        return self.substring(to: firstIndex).capitalized + self.substring(from: firstIndex).lowercased()
    }
    
    func onlyDigits() -> String {
        return self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
    }
    
    func substringPhoneNumber(_ phoneNumberCharCount: Int) -> String {
        if phoneNumberCharCount > 8 {
            return phoneNumberCharCount % 2 != 0 ? String(self.characters.suffix(9)) : String(self.characters.suffix(8))
        } else {
            return String(self.characters.suffix(phoneNumberCharCount))
        }
    }

    func formatPhoneNumber() -> String {
        var phoneNumberFormated = self
        
        if isPhoneNumberFree() {
            return phoneNumberFree()
        }
        
        switch self.characters.count {
        case 13:
            phoneNumberFormated = completeFormat(-11)
            break
        case 12:
            phoneNumberFormated = completeFormat(-10)
            break
        case 11:
            phoneNumberFormated = withoutNationCode(9)
            break
        case 10:
            phoneNumberFormated = withoutNationCode(8)
            break
        case 9:
            phoneNumberFormated = onlyPhoneNumber(5)
            break
        case 8:
            phoneNumberFormated = onlyPhoneNumber(4)
            break
        default:
            phoneNumberFormated = self
            break
        }
        return phoneNumberFormated
    }
    
    func completeFormat(_ offset: Int) -> String {
        _ = substring(to:self.index(self.endIndex, offsetBy: offset))
        let range = self.index(self.startIndex, offsetBy: 2)..<self.index(self.endIndex, offsetBy: offset+2)
        let region = self[range]
        let rangeA = self.index(self.startIndex, offsetBy: 4)..<self.index(self.endIndex, offsetBy: -4)
        let rangeB = self.index(self.startIndex, offsetBy: ((offset + 2) * -1))..<self.endIndex
        let phoneNumber = self[rangeA]+"-"+self[rangeB]
        return "(\(region))"+" \(phoneNumber)"
    }
    
    func withoutNationCode(_ offset: Int) -> String {
        let range = self.index(self.startIndex, offsetBy: 0)..<self.index(self.endIndex, offsetBy: 0 - offset)
        let region = self[range]
        let rangeA = self.index(self.startIndex, offsetBy: 2)..<self.index(self.endIndex, offsetBy: -4)
        let rangeB = self.index(self.startIndex, offsetBy: offset-2)..<self.endIndex
        let phoneNumber = self[rangeA]+"-"+self[rangeB]
        return "(\(region))"+" \(phoneNumber)"
    }
    
    func onlyPhoneNumber(_ offset: Int) -> String {
        let rangeA = self.index(self.startIndex, offsetBy: 0)..<self.index(self.endIndex, offsetBy: -4)
        let rangeB = self.index(self.startIndex, offsetBy: offset)..<self.endIndex
        let phoneNumber = self[rangeA]+"-"+self[rangeB]
        return phoneNumber
    }
    
    func isPhoneNumberFree() -> Bool {
        guard self.characters.count >= 4 else {
            return false
        }
        let range = self.index(self.startIndex, offsetBy: 0)..<self.index(self.endIndex, offsetBy: 4 - self.characters.count)
        let initial = self[range]
        return initial == "0300" || initial == "0500" || initial == "0800" || initial == "0900"
    }
    
    func phoneNumberFree() -> String {
        let range = self.index(self.startIndex, offsetBy: 0)..<self.index(self.endIndex, offsetBy: 4 - self.characters.count)
        let initial = self[range]
        let rangeB = self.index(self.startIndex, offsetBy: 4)..<self.index(self.endIndex, offsetBy: 7 - self.characters.count)
        let middle = self[rangeB]
        let rangeC = self.index(self.startIndex, offsetBy: 7)..<self.endIndex
        let final = self[rangeC]
        return "\(initial) "+"\(middle) "+"\(final)"
    }
    
}

extension UIImage {
    func imageWithInsets(insets: UIEdgeInsets) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.size.width + insets.left + insets.right,
                   height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let _ = UIGraphicsGetCurrentContext()
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets
    }
}
