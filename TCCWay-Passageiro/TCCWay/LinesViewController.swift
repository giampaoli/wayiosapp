//
//  LinesViewController.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 27/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import SwiftyBeaver

class LinesViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    
    var lines: [Line]? = nil
    
    let log = SwiftyBeaver.self
    fileprivate let presenter = LinePresenter(lineService: LineService())

    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController!.navigationBar.tintColor = UIColor.white
        
        presenter.attachView(view: self)
        presenter.getLines(driverId: String(User.user().id))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "lineDetails" ,
            let nextScene = segue.destination as? LineDetailsViewController ,
            let indexPath = self.tableView.indexPathForSelectedRow {

            let cell = self.tableView.cellForRow(at: indexPath) as? LineTableViewCell

            nextScene.lineImage = cell?.lineImage.image
            nextScene.line = (lines?[indexPath.row])!
        }
    }
}

extension LinesViewController: LinePresenterView {
    
    func startLoading() {
        log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func success(lines: [Line]) {
        log.info(lines)
        
        self.lines = lines
        tableView.reloadData()
    }
    
    func error(title: String, message: String) {
        log.error(message)
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
}
