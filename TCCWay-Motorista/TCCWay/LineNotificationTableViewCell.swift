//
//  NotificationTableViewCell.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 03/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class LineNotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var flowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func render(notification: Notification) {
        typeImage.image = notification.icon
        titleLabel.text = notification.title
        contentLabel.text = notification.content
    }
    
    func hiddenFlow() {
        flowView.isHidden = true
    }
    
    func showFlow() {
        flowView.isHidden = false
    }
    
}
