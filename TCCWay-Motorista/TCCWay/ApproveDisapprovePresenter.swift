//
//  ApproveDisapprovePresenter.swift
//  TCCWay-Motorista
//
//  Created by Jessica Batista de Barros Cherque on 21/10/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol ApproveDisapprovePresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(title: String, message: String)
    func error(title: String, message: String)
}

class ApproveDisapprovePresenter {
    
    let lineService: LineService?
    weak private var view: ApproveDisapprovePresenterView?
    
    init(lineService: LineService) {
        self.lineService = lineService
    }
    
    func attachView(view: ApproveDisapprovePresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func approveInputRequests(inputRequest : InputRequest){
        self.view?.startLoading()
        
        lineService?.approveInputRequests(inputRequest: inputRequest, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(title: "Aprovado com sucesso!!", message: "Agora \(inputRequest.name) pertece a sua linha.")
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
    
    func disapproveInputRequests(inputRequest : InputRequest){
        self.view?.startLoading()
        
        lineService?.disapproveInputRequests(inputRequest: inputRequest, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(title: "Removido com sucesso!!", message: "")
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}
