//
//  InputRequestTableViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 9/16/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import SwiftyBeaver

class InputRequestTableViewController: UITableViewController {
    
    var line = Line()
    var requests = [InputRequest]()
    var selectedRequest = InputRequest()
    
    let log = SwiftyBeaver.self
    fileprivate let presenter = InputRequestListPresenter(lineService: LineService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attachView(view: self)
        presenter.getInputRequests(lineId: "\(line.idLine)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        presenter.attachView(view: self)
        presenter.getInputRequests(lineId: "\(line.idLine)")
        reloadInputViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requests.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "inputRequest", for: indexPath) as? InputRequestTableViewCell
        
        cell?.render(request: requests[indexPath.row])
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRequest = requests[indexPath.row]
        self.performSegue(withIdentifier: "requestDetails", sender: self)
    }
    
     // MARK: - Navigation
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "requestDetails", let nextScene = segue.destination as? InputRequestDetailsViewController {
            nextScene.request = selectedRequest
        }
     }
}


extension InputRequestTableViewController: InputRequestListPresenterView {
    
    func startLoading() {
        log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func success(inputRequests: [InputRequest]) {
        log.info(inputRequests)
        
        self.requests = inputRequests
        tableView.reloadData()
    }
    
    func error(title: String, message: String) {
        log.error(message)
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
}
