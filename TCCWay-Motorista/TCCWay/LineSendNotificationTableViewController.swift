//
//  LineSendNotificationTableViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 08/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import SwiftyBeaver

class LineSendNotificationTableViewController: UITableViewController, UITextViewDelegate {
    
    @IBOutlet weak var sentTextView: UITextView!
    
    let log = SwiftyBeaver.self
    fileprivate let presenter = SentNotificationPresenter(notificationService: NotificationService())
    var line = Line()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attachView(view: self)
        
        sentTextView.text = "O que você gostaria de dizer aos Passageiros?"
        sentTextView.textColor = UIColor.lightGray
        sentTextView.delegate = self
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if sentTextView.textColor == UIColor.lightGray {
            sentTextView.text = nil
            sentTextView.textColor = UIColor.black
        }
        
        return true
    }
    
     func textViewDidEndEditing(_ textView: UITextView) {
        if sentTextView.text.isEmpty {
            sentTextView.text = "O que voce gostaria de dizer aos Passageiros?"
            sentTextView.textColor = UIColor.lightGray
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func sentMessage(_ sender: UIBarButtonItem) {
        print("Enviar")
        presenter.sentNotification(title: "Mensagem", content: sentTextView.text, type: "ic_notification", idLinha: line.idLine.description)
    }
}

extension LineSendNotificationTableViewController: SentNotificationPresenterView {
    func startLoading() {
         log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func success(notification: Notification) {
         log.info(notification)
         navigationController?.popViewController(animated: true)
    }
    
    func error(title: String, message: String) {
        log.error(message)
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
}
