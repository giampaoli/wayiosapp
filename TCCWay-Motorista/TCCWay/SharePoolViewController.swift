//
//  SharePoolViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 10/30/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class SharePoolViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func shareClick(_ sender: Any) {
        presentSucess()
    }
    
    private func presentSucess() {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: "Vaga divulgada com Sucesso",
            message: "",
            okFunction: callbackOk
        )
    }
    
    private func callbackOk() {
        navigationController?.popViewController(animated: true)
    }
}
