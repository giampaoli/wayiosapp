//
//  SentNotificationPresenter.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 11/1/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol SentNotificationPresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(notification: Notification)
    func error(title: String, message: String)
}

class SentNotificationPresenter {
    
    let notificationService: NotificationService?
    weak private var view: SentNotificationPresenterView?
    
    init(notificationService: NotificationService) {
        self.notificationService = notificationService
    }
    
    func attachView(view: SentNotificationPresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func sentNotification(title: String, content: String, type: String, idLinha: String) {
        self.view?.startLoading()
        
        notificationService?.sentNotification(title: title, content: content, type: type, idLinha: idLinha, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(notification: Notification(JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}
