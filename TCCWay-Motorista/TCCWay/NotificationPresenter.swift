//
//  NotificationPresenter.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 07/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol NotificationPresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(notifications: [Notification])
    func error(title: String, message: String)
}

class NotificationPresenter {
    
    let notificationService: NotificationService?
    weak private var view: NotificationPresenterView?
    
    init(notificationService: NotificationService) {
        self.notificationService = notificationService
    }
    
    func attachView(view: NotificationPresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func getNotifications(lineId: String) {
        self.view?.startLoading()
        
        notificationService?.getNotifications(lineId: lineId, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.success(notifications: [Notification](JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}

