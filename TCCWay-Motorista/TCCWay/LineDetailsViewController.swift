//
//  LineDetailsViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 04/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import MapKit

class LineDetailsViewController: UIViewController {
    
    @IBOutlet weak var imageVan: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var viewButtonPassenger: UIView!
    @IBOutlet weak var viewButtonTimeline: UIView!
    @IBOutlet weak var viewButtonInformations: UIView!
    
    var route = [MyAnnotation]()
    var lineImage: UIImage? = nil
    var line: Line? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO: Poderiamos não passar essa imagem assim e ja fazer download no momento do parse
        imageVan.image = lineImage
        
//        let userString = UserDefaults.standard.string(forKey: "route")
//
//        if userString == "1" {
//            setupRoute8()
//        } else {
//            setupRoute9()
//        }
//
//        setupMenuIntens()
//        setupMap()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.mapView.removeOverlays(mapView.overlays)
        self.route.removeAll()
        
        let userString = UserDefaults.standard.string(forKey: "route")
        
        if userString == "1" {
            setupRoute8()
        } else {
            setupRoute7()
        }
        
        setupMenuIntens()
        setupMap()
        setupNavigation()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController!.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController!.navigationBar.shadowImage = nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupNavigation() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = line?.title
    }
    
    func setupMenuIntens() {
        let gesturePassenger = UITapGestureRecognizer(target: self, action:  #selector (self.touchPassenger(sender:)))
        viewButtonPassenger.addGestureRecognizer(gesturePassenger)
        
        let gestureTimeline = UITapGestureRecognizer(target: self, action:  #selector (self.touchTimeline(sender:)))
        viewButtonTimeline.addGestureRecognizer(gestureTimeline)
        
        let gestureInformartions = UITapGestureRecognizer(target: self, action:  #selector (self.touchInformations(sender:)))
        viewButtonInformations.addGestureRecognizer(gestureInformartions)
    }
    
    func  touchPassenger(sender : UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "infoLines", sender: nil)
    }
    
    func  touchTimeline(sender : UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "timeline", sender: nil)
    }
    
    func  touchInformations(sender : UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "infoLines", sender: nil)
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segueID = segue.identifier {
            switch segueID {
            case "infoLines":
                let dest = segue.destination as! LineSpecificInfosTableViewController
                dest.line = line!
            case "timeline":
                let dest = segue.destination as! LineNotificationsTableViewController
                dest.line = line!
            case "startListPassangers":
                let dest = segue.destination as! StartPassangerViewController
                dest.line = line!
            default:
                break
            }
        }
    }
}

extension LineDetailsViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        renderer.lineWidth = 3
        return renderer
    }
    
    func setupMap() {
        let initialLocation = CLLocation(latitude: (route.first!.coordinate.latitude), longitude: (route.first!.coordinate.longitude))
        
        mapView.addAnnotations(self.route)
        
        
        self.mapView.centerMapOnLocation(location: initialLocation, latitudeDelta: 0.5, longitudeDelta: 0.5)
        self.mapView.zoomShowAllPins()
      
        DispatchQueue.global(qos: .userInitiated).async {
            self.mapView.drawDirectionsTo(route: self.arrayMyAnnotationToArrayCLLocationCoordinate2D(self.route))
        }
    }
    
    func arrayMyAnnotationToArrayCLLocationCoordinate2D(_ annotations: [MyAnnotation]) -> [CLLocationCoordinate2D] {
        var route: [CLLocationCoordinate2D] = []
        for pin in annotations{
            route.append(pin.coordinate)
        }
        return route
    }
    
    func setupRoute7() {
        var points: [ CLLocationCoordinate2D] = []
        points.append(CLLocationCoordinate2D(latitude:-22.86070184,longitude:-47.03663617)) //INICIAL
        points.append(CLLocationCoordinate2D(latitude:-22.86078093,longitude:-47.03379571))
        points.append(CLLocationCoordinate2D(latitude:-22.85774339,longitude:-47.03682125))
        points.append(CLLocationCoordinate2D(latitude:-22.85630493,longitude:-47.03762591))
        points.append(CLLocationCoordinate2D(latitude:-22.85442649,longitude:-47.03881145))
        points.append(CLLocationCoordinate2D(latitude:-22.85433998,longitude:-47.03886241))
        points.append(CLLocationCoordinate2D(latitude:-22.85189798,longitude:-47.04128712))
        points.append(CLLocationCoordinate2D(latitude:-22.85110456,longitude:-47.04242706))
        points.append(CLLocationCoordinate2D(latitude:-22.84958199,longitude:-47.04207301))
        points.append(CLLocationCoordinate2D(latitude:-22.8332676,longitude:-47.05182552)) //PUC
        
        route.append(MyAnnotation(title: "", id: "start", image: #imageLiteral(resourceName: "pin_start"), coordinate: points[0]) ) //INICIAL
//        route.append(MyAnnotation(title: "", id: "pass1", image: #imageLiteral(resourceName: "pass9"), coordinate: points[1]) )
        route.append(MyAnnotation(title: "", id: "pass2", image: #imageLiteral(resourceName: "pass3"), coordinate: points[2]) )
        route.append(MyAnnotation(title: "", id: "pass3", image: #imageLiteral(resourceName: "pass4"), coordinate: points[3]) )
        route.append(MyAnnotation(title: "", id: "pass4", image: #imageLiteral(resourceName: "pass6"), coordinate: points[4]) )
        route.append(MyAnnotation(title: "", id: "pass5", image: #imageLiteral(resourceName: "pass1"), coordinate: points[5]) )
        route.append(MyAnnotation(title: "", id: "pass6", image: #imageLiteral(resourceName: "pass8"), coordinate: points[6]) )
        route.append(MyAnnotation(title: "", id: "pass7", image: #imageLiteral(resourceName: "pass5"), coordinate: points[7]) )
        route.append(MyAnnotation(title: "", id: "pass8", image: #imageLiteral(resourceName: "pass2"), coordinate: points[8]) )
        route.append(MyAnnotation(title: "", id: "puc", image: #imageLiteral(resourceName: "pin_destino"), coordinate: points[9]) ) //PUC
    }
    
    func setupRoute8() {
        var points: [ CLLocationCoordinate2D] = []
        points.append(CLLocationCoordinate2D(latitude:-22.86070184,longitude:-47.03663617)) //INICIAL
        points.append(CLLocationCoordinate2D(latitude:-22.86078093,longitude:-47.03379571))
        points.append(CLLocationCoordinate2D(latitude:-22.85774339,longitude:-47.03682125))
        points.append(CLLocationCoordinate2D(latitude:-22.85630493,longitude:-47.03762591))
        points.append(CLLocationCoordinate2D(latitude:-22.85442649,longitude:-47.03881145))
        points.append(CLLocationCoordinate2D(latitude:-22.85433998,longitude:-47.03886241))
        points.append(CLLocationCoordinate2D(latitude:-22.85189798,longitude:-47.04128712))
        points.append(CLLocationCoordinate2D(latitude:-22.85110456,longitude:-47.04242706))
        points.append(CLLocationCoordinate2D(latitude:-22.84958199,longitude:-47.04207301))
        points.append(CLLocationCoordinate2D(latitude:-22.8332676,longitude:-47.05182552)) //PUC
        
        route.append(MyAnnotation(title: "", id: "start", image: #imageLiteral(resourceName: "pin_start"), coordinate: points[0]) ) //INICIAL
        route.append(MyAnnotation(title: "", id: "pass1", image: #imageLiteral(resourceName: "pass9"), coordinate: points[1]) )
        route.append(MyAnnotation(title: "", id: "pass2", image: #imageLiteral(resourceName: "pass3"), coordinate: points[2]) )
        route.append(MyAnnotation(title: "", id: "pass3", image: #imageLiteral(resourceName: "pass4"), coordinate: points[3]) )
        route.append(MyAnnotation(title: "", id: "pass4", image: #imageLiteral(resourceName: "pass6"), coordinate: points[4]) )
        route.append(MyAnnotation(title: "", id: "pass5", image: #imageLiteral(resourceName: "pass1"), coordinate: points[5]) )
        route.append(MyAnnotation(title: "", id: "pass6", image: #imageLiteral(resourceName: "pass8"), coordinate: points[6]) )
        route.append(MyAnnotation(title: "", id: "pass7", image: #imageLiteral(resourceName: "pass5"), coordinate: points[7]) )
        route.append(MyAnnotation(title: "", id: "pass8", image: #imageLiteral(resourceName: "pass2"), coordinate: points[8]) )
        route.append(MyAnnotation(title: "", id: "puc", image: #imageLiteral(resourceName: "pin_destino"), coordinate: points[9]) ) //PUC
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var viewReturn: MKAnnotationView
        if let annotation = annotation as? MyAnnotation {
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: annotation.id) {
                dequeuedView.annotation = annotation
                viewReturn = dequeuedView
            } else {
                viewReturn = MKAnnotationView(annotation: annotation, reuseIdentifier: annotation.id)
                viewReturn.image =  annotation.image
            }
        }else{
            viewReturn = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        }
        return viewReturn
    }
}


