//
//  MKMapView+search.swift
//  TCCWay-Passageiro
//
//  Created by Jessica Batista de Barros Cherque on 07/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import MapKit

extension MKMapView{
    
    func searchRequestTextMap( _ text:String, _ returningCoordinate: @escaping (CLLocationCoordinate2D!) -> ()){
        //Create the search request
        let searchRequest = MKLocalSearchRequest()
        searchRequest.naturalLanguageQuery = text
        
        let activeSearch = MKLocalSearch(request: searchRequest)
        
        activeSearch.start { (response, error) in
            
            if response == nil
            {
                returningCoordinate(nil)
            }
            else
            {
                //Getting data
                let latitude = response?.boundingRegion.center.latitude
                let longitude = response?.boundingRegion.center.longitude
                let coordinate = CLLocationCoordinate2DMake(latitude!, longitude!)
                
                returningCoordinate(coordinate)
            }
        }
    }

}
