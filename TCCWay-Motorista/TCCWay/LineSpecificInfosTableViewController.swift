//
//  LineSpecificInfosTableViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 07/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class LineSpecificInfosTableViewController: UITableViewController {

    var line = Line()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 2 : 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 120
        case 1:
            return 100
        default:
            return 120
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Membros"
        } else {
            return "Vagas"
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var identifier  = ""
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                identifier = "members"
            case 1:
                identifier = "newMember"
            default:
                identifier = "newMember"
            }
        } else {
            identifier = "vacancy"
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segueID = segue.identifier {
            switch segueID {
            case "listInputRequests":
                let dest = segue.destination as! InputRequestTableViewController
                dest.line = line
            default:
                break
            }
        }
    }
}
