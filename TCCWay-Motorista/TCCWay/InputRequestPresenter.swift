//
//  InputRequestListPresenter.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 9/16/17.
//  Copyright © 2017 TCC. All rights reserved.

//
import Foundation
import SwiftHTTP

protocol InputRequestListPresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func success(inputRequests: [InputRequest])
    func error(title: String, message: String)
}

class InputRequestListPresenter {
    
    let lineService: LineService?
    weak private var view: InputRequestListPresenterView?
    
    init(lineService: LineService) {
        self.lineService = lineService
    }
    
    func attachView(view: InputRequestListPresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func getInputRequests(lineId: String) {
        self.view?.startLoading()
        
        lineService?.getInputRequests(lineId: lineId, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        let p = response.text!
                        print(p)
                        self.view?.success(inputRequests: [InputRequest](JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
    
}
