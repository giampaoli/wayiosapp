//
//  AddLineService.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 15/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

class AddLineService {
    func setLine(line: Line, callback: @escaping (Response) -> Void) {
        print("SERVICE - setLine")
        
        let parameters = [
            "id_motorista": String(User.user().id),
            "titulo": line.title,
            "lat_inicial": String(line.inicialLocation!.latitude),
            "long_inicial": String(line.inicialLocation!.longitude),
            "lat_final": String(line.finalLocation!.latitude),
            "long_final": String(line.finalLocation!.longitude),
            "horario_chegada_destino": line.arrivalTime,
            "url_imagem": line.imageUrl,
            "dias_semana": line.daysWeek,
            "marca": line.vanBrand,
            "modelo": line.vanModel,
            "horario_saida": line.arrivalTime,
            "data_pagamento": "01/01/2017"
        ]
        
        let urlString = APIConstants.getBaseURL() + APIConstants.add_new_line

        print(urlString)
        
        do {
            let opt = try HTTP.POST(urlString, parameters: parameters, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
}
