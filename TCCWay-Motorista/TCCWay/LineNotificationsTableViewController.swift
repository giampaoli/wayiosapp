//
//  TimeLineTableViewController.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 03/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import SwiftyBeaver

class LineNotificationsTableViewController: UITableViewController {
    
    var notifications: [Notification]? = nil
    var line = Line()
    
    let log = SwiftyBeaver.self
    fileprivate let presenter = NotificationPresenter(notificationService: NotificationService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attachView(view: self)
        self.tableView.sectionHeaderHeight = 60
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.getNotifications(lineId: line.idLine.description)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications != nil ? (notifications?.count)! : 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notification", for: indexPath) as? LineNotificationTableViewCell
        
        cell?.render(notification: (notifications?[indexPath.row])!)
        
        if indexPath.row == (notifications?.count)! - 1 {
            cell?.hiddenFlow()
        } else {
             cell?.showFlow()
        }
        
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sentNotification" ,
            let nextScene = segue.destination as? LineSendNotificationTableViewController {
            nextScene.line = line
        } 
    }
}

extension LineNotificationsTableViewController: NotificationPresenterView {
    
    func startLoading() {
        log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func success(notifications: [Notification]) {
        log.info(notifications)
        
        self.notifications = notifications
        tableView.reloadData()
    }
    
    func error(title: String, message: String) {
        log.error(message)
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "headerNotifications")
        return header
    }
}

