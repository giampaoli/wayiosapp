//
//  InputRequestDetailsViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 9/16/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MapKit
import SwiftyBeaver

class InputRequestDetailsViewController: UIViewController, MKMapViewDelegate {
    
    let log = SwiftyBeaver.self
    @IBOutlet weak var restaurarAddressButton: UIButton!
    var requestInicial = InputRequest()
    var request = InputRequest()
    fileprivate let presenter = ApproveDisapprovePresenter(lineService: LineService())

    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var vaiLabel: UILabel!
    @IBOutlet weak var voltaLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.restaurarAddressButton.isEnabled = true
        presenter.attachView(view: self)
        map.delegate = self
        requestInicial = request
        geoCode(location: CLLocation(latitude: request!.latitude, longitude: request!.longitude))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func touchRestaurarAdressButton(_ sender: Any) {
        request = requestInicial
        self.restaurarAddressButton.isEnabled = false
        setupView()
    }
    
    func setupView() {
        geoCode(location: CLLocation(latitude: request!.latitude, longitude: request!.longitude))
        
//        map.zoomingPoint(request!.location)
        let span = MKCoordinateSpanMake(0.01, 0.01)
        let region = MKCoordinateRegionMake((request!.location, span))
        map.setRegion(region, animated: true)
            
            
        
        nameLabel.text = request?.name
        vaiLabel.text   = "Vai:   Dom Seg Ter Qua Qui Sex Sáb "
        voltaLabel.text = "Volta: Dom Seg Ter Qua Qui Sex Sáb "
        
        imageContainerView.cornerRadius = Double(imageContainerView.frame.width / 2)
        imageContainerView.clipsToBounds = true
        mainImageView.cornerRadius = Double(mainImageView.frame.width / 2)
        mainImageView.clipsBounds = true
        
        Alamofire.request((request?.urlPictureUser)!).responseImage { response in
            debugPrint(response)
            if let image = response.result.value {
                print("image downloaded: \(image)")
                self.mainImageView.image = image
            }
        }
    }

    func geoCode(location : CLLocation!){
        /* Only one reverse geocoding can be in progress at a time hence we need to cancel existing
         one if we are getting location updates */
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (data, error) -> Void in
            guard let placeMarks = data as [CLPlacemark]! else {
                return
            }
            let loc: CLPlacemark = placeMarks[0]
            let addressDict : [NSString:NSObject] = loc.addressDictionary as! [NSString: NSObject]
            let addrList = addressDict["FormattedAddressLines"] as! [String]
            let address = addrList.joined(separator: ", ")
            self.addressLabel.text = address
        })
    }
    @IBAction func touchAprovar(_ sender: Any) {
        self.presenter.approveInputRequests(inputRequest: request!)
        
        UserDefaults.standard.set("1", forKey: "route")
        UserDefaults.standard.synchronize()
        
    }
    
    @IBAction func touchReprovar(_ sender: Any) {
        self.presenter.disapproveInputRequests(inputRequest: request!)
        
        UserDefaults.standard.set("0", forKey: "route")
        UserDefaults.standard.synchronize()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.restaurarAddressButton.isEnabled = true
        let coordinate = mapView.centerCoordinate
        geoCode(location: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
    }
    
}

extension InputRequestDetailsViewController: ApproveDisapprovePresenterView{
    
    func startLoading() {
        log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func success(title: String, message: String) {
        log.info(message)
        alert(title: title, message: message)
    }
    
    func error(title: String, message: String) {
        log.error(title)
        alert(title: title, message: message)

    }
    
    func alert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.self.navigationController?.popViewController(animated: true)
        }))
        present(alert, animated: true, completion: nil)

    }
}

