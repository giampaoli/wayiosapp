//
//  PassengersViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 05/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import Contacts

class LineInformationsViewController: UIViewController {
    
    let contacts = Contacts.sharedInstance.getContacts()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(contacts)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
