//
//  DaysOfWeekTableViewCell.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 09/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class DaysOfWeekTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dayOfWeekLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func render(dayOfWeek: String) {
        self.selectionStyle = .none
        
        if self.isSelected {
            self.accessoryType = .checkmark
        } else {
            self.accessoryType = .none
        }
        
        dayOfWeekLabel.text = dayOfWeek
    }
}
