//
//  CreateLineSearchBrandModelViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 13/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import SwiftyBeaver

class CreateLineSearchBrandModelViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    public var fipe = [Fipe]()
    public var fiteredFipe = [Fipe]()
    
    public var funcResult: ((Fipe)->())!
    public var brandSelected = Fipe()
    public var type = ""
    
    fileprivate let presenter = FipePresenter(fipeService: FipeService())
    let log = SwiftyBeaver.self
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if type == "segueBrand" {
            presenter.getBrands()
        } else {
            presenter.getModels(brandId: (brandSelected?.cod)!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension CreateLineSearchBrandModelViewController: FipePresenterView {
    
    func startLoading() {
        log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func successBrands(brands: [Fipe]) {
        log.info(brands)
        fipe = brands
        fiteredFipe = brands
        
        tableView.reloadData()
    }
    
    func successModels(models: [Fipe]) {
        log.info(models)
        fipe = models
        fiteredFipe =  models
        
        tableView.reloadData()
    }
    
    func error(title: String, message: String) {
        log.error("\(title) \(error)")
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
}

extension CreateLineSearchBrandModelViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fiteredFipe.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = fiteredFipe[indexPath.row].name
        
        return cell
    }
}

extension CreateLineSearchBrandModelViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let selectedFipe = fiteredFipe[indexPath.row]
        self.funcResult(selectedFipe)
        
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}

extension CreateLineSearchBrandModelViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.fiteredFipe =  searchText.isEmpty ? fipe : fipe.filter {$0.name.range(of: searchText, options: [.anchored, .caseInsensitive, .diacriticInsensitive]) != nil }
        tableView.reloadData()
    }
}

