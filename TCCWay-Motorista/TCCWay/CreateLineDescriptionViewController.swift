//
//  CreateLineDescriptionViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 09/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class CreateLineDescriptionViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var pickerHorarioChegada: UIDatePicker!
    
     var imageUploadManager: ImageUploadManager?
    var line = Line()
    
    var daysWeekBool = [false, false, false, false, false, false, false]
    let daysWeek = ["Todo Domingo", "Toda Segunda", "Toda Terça", "Toda Quarta", "Toda Quinta", "Toda Sexta", "Todo Sabado"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func nextViewController(_ sender: Any) {
        setLine()
        self.performSegue(withIdentifier: "nextMasterPoints", sender: nil)
    }
    
    func setLine(){
        line.setIdDriver(User.user().id)
        line.setTitle(title: self.titleTextField.text!)
//        line.setImageUrl("http://www.country1067.com/wp-content/blogs.dir/33/files/scooby-doo-cast.jpg")
        line.setDaysWeek( daysWeekToString() )
        line.setArrivalTime( getHourFromDatePicker())
        line.setArrivalTime( getHourFromDatePicker())
    }
    
    func getHourFromDatePicker() -> String
    {
        let picker = pickerHorarioChegada
        
        let date = picker!.date
        let components = Calendar.current.dateComponents([.hour, .minute], from: date)
        let hour = components.hour!
        let minute = components.minute!
        return "\(hour):\(minute)"
    }
    
    func daysWeekToString() -> String{
        var daysString = ""
        var day = 1
        for daybool in daysWeekBool {
            if daybool{
                daysString = "\(daysString) \(day),"
            }
            day += 1
        }
        if(daysString != ""){
            daysString.remove(at: daysString.index(before: daysString.endIndex))
        }
        return daysString
    }
    // MARK: - Navigation
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "nextMasterPoints" ,
            let nextScene = segue.destination as? CreateLineMasterPointsViewController {
            nextScene.line = line
        }
    }
    
    @IBAction func loadImageButton(_ sender: Any) {
        
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerControllerSourceType.photoLibrary
        image.allowsEditing =  false
        
        self.present(image, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            contentImageView.image = image
            iconImageView.isHidden = true
            self.uploadImage(image: image)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func uploadImage(image: UIImage) {
        imageUploadManager = ImageUploadManager()
        imageUploadManager?.uploadImage(image, progressBlock: { (percentage) in
            print(percentage)
        }, completionBlock: { [weak self] (fileURL, errorMessage) in
            guard let strongSelf = self else {
                print("error \(String(describing: errorMessage))")
                return
            }
            print(fileURL!.description)
            self?.line.setImageUrl(fileURL!.description)
        })
    }
    

    @IBAction func unwindToStep1(segue:UIStoryboardSegue) {
        self.dismiss(animated: true, completion: nil)
        self.performSegue(withIdentifier: "showDetail", sender: self)
        
    }
}
