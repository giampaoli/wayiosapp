//
//  ViewController.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 24/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import SwiftyBeaver

class VerifyViewController: UIViewController {
    
    @IBOutlet weak var textNumber: PhoneTextField!
    @IBOutlet weak var buttonValidate: UIButton!
    
    let log = SwiftyBeaver.self
    fileprivate let presenter = VerifyPresenter(verifyService: VerifyService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attachView(view: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func touchValidadeButton(_ sender: Any) {
        presenter.postVerify(telephone: "11971086251")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTokenViewController" {
            let tokenVC = segue.destination as! TokenViewController;
            tokenVC.telephone = "11971086251"
        }
    }
}

extension VerifyViewController: VerifyPresenterView {
    
    func startLoading() {
        log.verbose("Start Loading")
        buttonValidate.loadingIndicator(show: true)
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
        buttonValidate.loadingIndicator(show: false)
    }
    
    func success(verify: Verify) {
        log.info(verify)
        buttonValidate.loadingIndicator(show: false)
        self.performSegue(withIdentifier: "toTokenViewController", sender: nil)
    }
    
    func error(title: String, message: String) {
        log.error("\(title) \(error)")
        buttonValidate.loadingIndicator(show: false)
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
}


