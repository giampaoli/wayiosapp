//
//  FormattedTextField.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 24/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class FormattedTextField: UITextField, UITextFieldDelegate {
    public var formatRules: FormattedTextFieldRules?
    
    init() {
        super.init(frame: CGRect())
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize() {
        addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    @objc private func textFieldDidChange(textField: FormattedTextField) {
        formatRules?.format(textField: textField)
        let isFinished = formatRules?.isFinished(textField: textField) ?? false
        if isFinished {
            resignFirstResponder()
            formattedDelegate?.textFieldDidFinshEditing(textField: textField)
        }
    }
    
    func unformattedText() -> String! {
        return formatRules?.unformatted(string: self.text ?? "") ?? ""
    }
    
    func isValid() -> Bool {
        return formatRules?.isValid(textField: self) ?? false
    }
    
    func setDelegate(_ delegate: FormattedTextFieldDelegate?) {
        formattedDelegate = delegate
    }
    
    // MARK: - Private Variables
    
    internal weak var formattedDelegate: FormattedTextFieldDelegate?
}
