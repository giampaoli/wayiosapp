//
//  ProfileTabViewController.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 29/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class ProfileTabViewController: TabsViewController {
    
    override func viewDidLoad() {
        storyboardName = "Profile"
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
