//
//  CreateLineMasterPointsViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 09/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import MapKit

class CreateLineMasterPointsViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var textPointStart: UILabel!
    @IBOutlet weak var textPointDestiny: UILabel!
    
    var pinStarting :MKPointAnnotation!
    var pinDestiny :MKPointAnnotation!
    var line = Line()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func nextViewController(_ sender: Any) {
        if pinStarting != nil && pinDestiny != nil {
            line.setInicialPoint(point: Point(location: pinStarting.coordinate))
            line.setFinalPoint(point: Point(location: pinDestiny.coordinate))
            self.performSegue(withIdentifier: "nextVanDescription", sender: nil)
        }
        
    }
    
    func setupPinDestiny(_ pin: MKPointAnnotation){
        self.pinDestiny = pin
        self.textPointDestiny.text = pin.title
        reloadDataMap()
    }
    
    func setupPinStarting(_ pin: MKPointAnnotation){
        self.pinStarting = pin
        self.textPointStart.text = pin.title
        reloadDataMap()
    }
    
    func reloadDataMap(){
        if pinStarting != nil || pinDestiny != nil {
            let annotations = mapView.annotations
            mapView.removeAnnotations(annotations)
        }
        
        if pinStarting != nil{
            mapView.addAnnotation(pinStarting)
        }
        
        if pinDestiny != nil{
            mapView.addAnnotation(pinDestiny)
        }
        
        if pinStarting != nil && pinDestiny != nil {
            mapView.zoomShowAllPins()
        }else{
            if pinStarting != nil{
                mapView.zoomingPoint(pinStarting.coordinate)
            }
            
            if pinDestiny != nil{
                mapView.zoomingPoint(pinDestiny.coordinate)
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segueID = segue.identifier {
            switch segueID {
            case "segueStarting":
                let dest = segue.destination as! SearchLocationViewController
                dest.funcResult = self.setupPinStarting
                dest.navigationItem.title = "Ponto Inicial"
            case "segueDestiny":
                let dest = segue.destination as! SearchLocationViewController
                dest.funcResult = self.setupPinDestiny
                dest.navigationItem.title = "Destino/ Universidade"
            case "nextVanDescription":
                if let nextScene = segue.destination as? CreateLineVanDescriptionViewController {
                    nextScene.line = line
                }
            default:
                break
            }
        }
    }
    
}
