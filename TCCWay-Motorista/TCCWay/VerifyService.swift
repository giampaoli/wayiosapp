//
//  LoginService.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 24/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//
//

import Foundation
import SwiftHTTP
import SwiftyBeaver

class VerifyService {
    let log = SwiftyBeaver.self
    
    func postVerify(telephoneNumber: String, callback: @escaping (Response) -> Void) {
        let urlString = APIConstants.getBaseURL() + APIConstants.auth_verify_number;
        let parameter = ["telefone": telephoneNumber];
        
        log.info("parameter \(parameter.description)")
        
        do {
            let opt = try HTTP.POST(urlString, parameters: parameter, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    self.log.error("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            log.error("Got an error creating the request: \(error)")
        }
    }
}
