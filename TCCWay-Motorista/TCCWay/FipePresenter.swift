//
//  FipePresenter.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 13/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol FipePresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func successBrands(brands: [Fipe])
    func successModels(models: [Fipe])
    func error(title: String, message: String)
}

class FipePresenter {
    
    let fipeService: FipeService?
    weak private var view: FipePresenterView?
    
    init(fipeService: FipeService) {
        self.fipeService = fipeService
    }
    
    func attachView(view: FipePresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func getBrands() {
        self.view?.startLoading()
        
        fipeService?.getBrands(callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.successBrands(brands: [Fipe](JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
    
    func getModels(brandId: Int) {
        self.view?.startLoading()
        
        fipeService?.getModels(brandId: brandId, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        let modelFipe = [Fipe](JSONString: response.text!)!
                        self.view?.successModels(models: modelFipe)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }
}
