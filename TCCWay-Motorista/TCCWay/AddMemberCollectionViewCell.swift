//
//  AddMemberCollectionViewCell.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 08/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class AddMemberCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func render(contact: Contact) {
        nameLabel.text = contact.name
        addBorderColor(color: .orange)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.backgroundColor = .clear
        
        imageContainerView.cornerRadius = Double(imageContainerView.frame.width / 2)
        imageContainerView.clipsToBounds = true
        mainImageView.cornerRadius = Double(mainImageView.frame.width / 2)
        mainImageView.clipsBounds = true
    }
    
    func addBorderColor(color: UIColor) {
        imageContainerView.borderWidth = Double((1.5 / 29.5) * mainImageView.frame.size.width)
        imageContainerView.borderColor = color
    }
}
