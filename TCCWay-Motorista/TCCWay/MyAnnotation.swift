//
//  MyAnnotations.swift
//  TCCWay-Passageiro
//
//  Created by Jessica Batista de Barros Cherque on 06/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//


import Foundation

import MapKit

// TODO: fazer aqui uma forma simples para retornar a Annotation adequada para cada situação.

enum AnnotationType{
    case pontoInicial
    case pontoDestino
    case pontoPassageiro
    case myPonto
}

class MyAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var image: UIImage
    var id: String
    let title: String?
    
    init(title: String, id: String, image: UIImage, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.image = image
        self.id = id
        self.coordinate = coordinate
        super.init()
    }
    
    var subtitle: String? {
        return title
    }
}

