//
//  AddMembersTableViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 08/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class AddMembersTableViewController: UITableViewController {
    
    var contacts = Contacts.sharedInstance.getContacts().sorted(by: { $0.name! < $1.name! })
    
    var selectedContacts : Array =  [Contact]()

    var headerCollection: AddMemberTableViewHeader? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.sectionHeaderHeight = 80
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addMember", for: indexPath) as? AddMemberTableViewCell
        
        cell?.render(contact: contacts[indexPath.row])
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        
        selectedContacts.append(contacts[indexPath.row])
        
        headerCollection?.selectedContacts = selectedContacts
        headerCollection?.reloadCollection()
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        cell.accessoryType = .none
        
        let contact = contacts[indexPath.row]
        selectedContacts.remove(object: contact)
        
        headerCollection?.selectedContacts = selectedContacts
        headerCollection?.reloadCollection()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        headerCollection = tableView.dequeueReusableCell(withIdentifier: "header") as? AddMemberTableViewHeader
        return headerCollection
    }
    
    
    @IBAction func tapSend(_ sender: UIBarButtonItem) {
        presentSucess()
    }
    
    private func presentSucess() {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: "Convite Enviado",
            message: "Enviamos um convite para SMS para os passageiros selecionados!",
            okFunction: callbackOk
        )
    }
    
    private func callbackOk() {
        navigationController?.popViewController(animated: true)
    }
}
