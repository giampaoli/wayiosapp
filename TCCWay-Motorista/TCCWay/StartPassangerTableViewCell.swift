//
//  StartPassangerTableViewCell.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 11/4/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class StartPassangerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.backgroundColor = .clear
        
        imageContainerView.cornerRadius = Double(imageContainerView.frame.width / 2)
        imageContainerView.clipsToBounds = true
        mainImageView.cornerRadius = Double(mainImageView.frame.width / 2)
        mainImageView.clipsBounds = true
    }
    
    func render(user: User){
        self.selectionStyle = .none
        
        nameLabel.text = user.name
        
        Alamofire.request(user.url_imagem).responseImage { response in
            debugPrint(response)
            if let image = response.result.value {
                print("image downloaded: \(image)")
                self.mainImageView.image = image
            }
        }
        
        addBorderColor(color: #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1))
    }
    
    func addBorderColor(color: UIColor) {
        imageContainerView.borderWidth = Double((1.5 / 29.5) * mainImageView.frame.size.width)
        imageContainerView.borderColor = color
    }

}
