
import UIKit

struct ErrorAlert {
    
    func showWith(controller: UIViewController,
                  title: String,
                  message: String,
                  reloadFunction: @escaping () -> Void,
                  cancelButtonTitle: String? = "Cancel",
                  cancelFunction: (() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction: UIAlertAction!
        if let cancelFunction = cancelFunction {
            cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { _ -> Void in
                cancelFunction()
            })
        } else {
            cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { _ -> Void in
                controller.dismiss(animated: true, completion: nil)
            })
        }
        alertController.addAction(cancelAction)
        
        let tryAgainAction = UIAlertAction(title: "Try Again", style: .default, handler: { _ -> Void in
            reloadFunction()
        })
        alertController.addAction(tryAgainAction)
        
        DispatchQueue.main.async {
            controller.present(alertController, animated: true, completion: nil)
        }
    }
}
