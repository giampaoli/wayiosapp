//
//  LinesViewController+UITableView.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 29/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

extension LinesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lines != nil ? (lines?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "line", for: indexPath) as? LineTableViewCell
        
        cell?.render(line: (lines?[indexPath.row])!)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         self.performSegue(withIdentifier: "lineDetails", sender: self)
    }
    
}
