//
//  StartPassangerViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 11/4/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import SwiftyBeaver
import MapKit

class StartPassangerViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let log = SwiftyBeaver.self
    fileprivate let presenter = MembersPresenter(lineService: LineService())
    
    var line = Line()
    var members: [User]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attachView(view: self)
        
        presenter.getMembers(lineId: line.idLine.description)
//        presenter.getMembers(lineId: "101")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func clickStart(_ sender: Any) {
       openMapForPlace()
    }
    
    func openMapForPlace() {
        
        let latitude: CLLocationDegrees = -22.8337547
        let longitude: CLLocationDegrees = -47.0481732
        
        let regionDistance:CLLocationDistance = 1000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Puc Campinas - (8 Passageiros)"
        mapItem.openInMaps(launchOptions: options)
    }
    

}

extension  StartPassangerViewController: UITableViewDelegate, UITableViewDataSource { func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
     return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members == nil ? 0 : members!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "member", for: indexPath) as? StartPassangerTableViewCell
        
         cell?.render(user: members![indexPath.row])
        
        return cell!
    }
}

extension StartPassangerViewController: MemberPresenterView {
    
    func startLoading() {
        log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func success(users: [User]) {
        log.info(users)
        self.members = users
        tableView.reloadData()
    }
    
    func error(title: String, message: String) {
        log.error(message)
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
}
