//
//  LinesTabViewController.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 29/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class LinesTabViewController: TabsViewController {
    
    override func viewDidLoad() {
        storyboardName = "Lines"
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
