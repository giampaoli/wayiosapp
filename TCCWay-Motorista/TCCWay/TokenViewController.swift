//
//  TokenViewController.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 25/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class TokenViewController: UIViewController {
    
    
    @IBOutlet weak var tokenText: TokenTextField!
    @IBOutlet weak var validateTokenButton: UIButton!
    
    var telephone: String!
    fileprivate let presenter = LoginPresenter(loginService: LoginService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = telephone;
        
        presenter.attachView(view: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func touchValidadeButton(_ sender: Any) {
        //        presenter.postLogin(token: tokenText.text!.replacingOccurrences(of: " ", with: "") , telephone: telephone)
        presenter.postLogin(token:"111111" , telephone: telephone)
    }
    
    func navigateToHome() {
        let targetStoryboardName = "Home"
        let targetStoryboard = UIStoryboard(name: targetStoryboardName, bundle: nil)
        if let targetViewController = targetStoryboard.instantiateInitialViewController() {
            self.navigationController?.present(targetViewController, animated: true, completion: nil)
        }
    }
}

extension TokenViewController: LoginPresenterView {
    
    func startLoading() {
        print("Start Loading")
        validateTokenButton.loadingIndicator(show: true)
    }
    
    func finishLoading() {
        print("Finish Loading")
        validateTokenButton.loadingIndicator(show: false)
    }
    
    func success(user: User) {
        print(user)
        User.login(json: user.toJSONString()!)
        validateTokenButton.loadingIndicator(show: false)
        
        navigateToHome()
    }
    
    func error(title: String, message: String) {
        print(message)
        validateTokenButton.loadingIndicator(show: false)
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
}
