//
//  Array+RemoveObject.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 08/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation

extension Array where Element: AnyObject {
    mutating func remove(object: Element) {
        if let index = index(where: { $0 === object }) {
            remove(at: index)
        }
    }
}
