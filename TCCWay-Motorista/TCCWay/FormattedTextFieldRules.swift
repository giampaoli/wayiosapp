import UIKit

class FormattedTextFieldRules {
    
    func genericCharacter() -> String {
        fatalError()
    }
    
    func desiredFormat(string: String) -> String {
        fatalError()
    }
    
    func unformatted(string: String) -> String {
        return StringFormatter.unformatted(string: string,
                                           format: desiredFormat(string: string),
                                           genericCharacter: genericCharacter())
    }
    
    func format(string: String) -> String {
        let unformattedText = self.unformatted(string: string)
        let desiredFormat = self.desiredFormat(string: unformattedText)
        return StringFormatter.formatted(string: unformattedText,
                                         format: desiredFormat,
                                         genericCharacter: genericCharacter())
    }
    
    func format(textField: UITextField) {
        let formattedText = self.format(string: textField.text ?? "")
        if textField.text != formattedText {
            textField.text = formattedText
        }
    }
    
    func isFinished(textField: UITextField) -> Bool {
        let unformattedCurrentText = self.unformatted(string: textField.text ?? "")
        let desiredFormat = self.desiredFormat(string: unformattedCurrentText)
        let unformattedExpectedText = self.unformatted(string: desiredFormat)
        return !(unformattedCurrentText.length < unformattedExpectedText.length)
    }
    
    func isValid(textField: UITextField) -> Bool {
        let unformattedCurrentText = self.unformatted(string: textField.text ?? "")
        let desiredFormat = self.desiredFormat(string: unformattedCurrentText)
        let unformattedExpectedText = self.unformatted(string: desiredFormat)
        return unformattedCurrentText.length == 0 || unformattedCurrentText.length == unformattedExpectedText.length
    }
}
