//
//  CreateLineVanDescriptionViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 09/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class CreateLineVanDescriptionViewController: UIViewController {
    
    @IBOutlet weak var vanBrandLabel: UILabel!
    @IBOutlet weak var vanModelLabel: UILabel!
    
    fileprivate let presenter = AddLinePresenter(addLineService: AddLineService())
    
    var line = Line()
    
    public var brandSelected = Fipe()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func doneAddLine(_ sender: Any){
        //TODO: MANDAR PARA O SERVIDOR
        line.setVanBrand( vanBrandLabel.text!)
        line.setVanModel( vanModelLabel.text!)

        presenter.addLine(line: line)
        
    }
    
    
    func setupBrand(_ fipe: Fipe){
        print(fipe)
        brandSelected = fipe
        vanBrandLabel.text = fipe.name
        vanModelLabel.text = ""
    }
    
    func setupModel(_ fipe: Fipe){
        print(fipe)
        vanModelLabel.text = fipe.name
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        ///if segue.identifier == "nextPassengers", let nextScene = segue.destination as? CreateLinePassengersViewController {
        ///    nextScene.line = line
        ///} else
        if  segue.identifier == "segueBrand", let nextScene = segue.destination as? CreateLineSearchBrandModelViewController {
            nextScene.type = segue.identifier!
            nextScene.funcResult = setupBrand(_:)
        } else if  segue.identifier == "segueModel", let nextScene = segue.destination as? CreateLineSearchBrandModelViewController {
            nextScene.type = segue.identifier!
            nextScene.funcResult = setupModel(_:)
            nextScene.brandSelected = brandSelected
        }
    }
}

extension CreateLineVanDescriptionViewController: AddLinePresenterView {
    func startLoading() {
        //
    }
    
    func finishLoading() {
        //
    }
    
    func successAddLine(line: Line) {
        performSegue(withIdentifier: "unwindSegueToStep1", sender: self)
        
    }
    
    func error(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
    
    
}
