//
//  AddLinePresenter.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 13/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

protocol AddLinePresenterView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func successAddLine(line: Line)
    func error(title: String, message: String)
}

class AddLinePresenter {
    
    let addLineService: AddLineService?
    weak private var view: AddLinePresenterView?
    
    init(addLineService: AddLineService) {
        self.addLineService = addLineService
    }
    
    func attachView(view: AddLinePresenterView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func addLine( line: Line) {
        self.view?.startLoading()
        
        addLineService?.setLine(line: line, callback: { (response) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if let statusCode = HTTPStatusCodes(rawValue: (response.statusCode != nil) ?  response.statusCode! : (response.error?.code)!) {
                    switch (statusCode) {
                    case .success:
                        self.view?.successAddLine(line: Line(JSONString: response.text!)!)
                    default:
                        self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                    }
                } else {
                    self.view?.error(title: "Problema encontrado! :(", message: "Estamos nos empenhando para restaurar a sua conexão com nosso sistema. Tente novamente em alguns instantes.")
                }
            })
        })
    }

}
