//
//  FipeService.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 13/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

class FipeService {
    func getBrands(callback: @escaping (Response) -> Void) {
        print("SERVICE - getLines")
        
        let urlString = "https://fipeapi.appspot.com/api/1/carros/marcas.json"
        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
    
    func getModels(brandId: Int, callback: @escaping (Response) -> Void) {
        print("SERVICE - getLines")
        
//        let urlString = "http://fipe-parallelum.rhcloud.com/api/v1/carros/marcas/\(brandId)/modelos"
        
        let urlString = "https://fipeapi.appspot.com/api/1/carros/veiculos/\(brandId).json"
        
        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
}
