//
//  AddMemberTableViewHeader.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 08/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

class AddMemberTableViewHeader: UITableViewCell,  UICollectionViewDataSource {
    
    var selectedContacts = [Contact]()
     @IBOutlet weak var collection: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedContacts.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionMember", for: indexPath) as! AddMemberCollectionViewCell
        
        cell.render(contact: selectedContacts[indexPath.row])
        
        return cell
    }
    
    func reloadCollection() {
        collection.reloadData()
    }
}
