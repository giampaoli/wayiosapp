//
//  Line.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 29/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import ObjectMapper
import MapKit

struct Line: Mappable {
    
    private(set) var idLine = 0
    private(set) var idDriver = 0
    private(set) var inicialLocation: Point? = nil
    private(set) var finalLocation: Point? = nil
    private(set) var title = ""
    private(set) var arrivalTime = ""
    private(set) var imageUrl = ""
    private(set) var paymentDate = ""
    private(set) var passengers = [Contact]()
    private(set) var vanBrand = ""
    private(set) var vanModel = ""
    private(set) var route: [CLLocationCoordinate2D]?
    private(set) var daysWeek = ""
    
    init?(map: Map) {}
    init() {}
    
    mutating func mapping(map: Map) {
        idLine <- map["id"]
        idDriver <- map["id_motorista"]
        inicialLocation <- map["id_localizacao_inicial"]
        finalLocation <- map["id_localizacao_final"]
        title <- map["titulo"]
        arrivalTime <- map["horario_chegada_destino"]
        paymentDate <- map["data_pagamento"]
        imageUrl <- map["url_imagem"]
        vanBrand <- map["marca"]
        vanModel <- map["model"]
        daysWeek <- map["daysWeek"]
    }
    
    mutating func setInicialLocation(_ point: Point){
        self.inicialLocation = point
    }
    
    mutating func setFinalLocation(_ point: Point){
        self.finalLocation = point
    }
    
    mutating func setTitle(title: String) {
        self.title = title
    }
    
    mutating func setPassengers(passengers: [Contact]) {
        self.passengers = passengers
    }
    
    mutating func setInicialPoint(point: Point) {
        self.inicialLocation = point
    }
    
    mutating func setFinalPoint(point: Point) {
        self.finalLocation = point
    }
    
    mutating func setIdDriver(_ id: Int) {
        self.idDriver = id
    }
    
    mutating func setArrivalTime(_ time: String) {
        self.arrivalTime = time
    }
    
    mutating func setImageUrl(_ url: String) {
        self.imageUrl = url
    }
    
    mutating func setPaymentDate(_ date: String) {
        self.paymentDate = date
    }
    
    mutating func setVanBrand(_ brand: String) {
        self.vanBrand = brand
    }
    
    mutating func setVanModel(_ model: String) {
        self.vanModel = model
    }
    
    mutating func setDaysWeek(_ days: String) {
        self.daysWeek = days
    }
}
