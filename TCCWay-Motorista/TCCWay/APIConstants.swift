//
//  APIConstants.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 25/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation

enum Endpoint {
    case production
    case develop
}

class APIConstants {
    
    static var endpoint: Endpoint = .develop
    
    //Line
    static var lines_by_driver = "/api/linhas/"
    static var add_new_line = "/api/linha/registrar"
    static var input_request_line = "/api/motorista/solicitacoes/"
    static var approve_input_request = "/api/motorista/aprovar-solicitacao"
    static var disapprove_input_request = "/api/motorista/reprovar-solicitacao"
    static var passenger_by_line = "/api/passageiros/linhas/"
    
    //Line Notifications
    static var notifications_by_line = "/api/linha/notificacoes/linha/"
    static var add_notifications_by_line = "/api/linha/notificacoes"
    
    //Login
    static var auth_verify_number = "/api/auth/verify"
    static var auth_login = "/api/auth/login"
    
    
    
    static func getBaseURL() -> String {
        
        switch APIConstants.endpoint {
        case .develop:
            return "http://localhost:3000"
//            return "http://192.168.0.22:3000"
        case .production:
            return "http://mastsolucoes.com:5001"
        }
    }
}
