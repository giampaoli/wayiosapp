//
//  HTTPStatusCode.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 25/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation

enum HTTPStatusCodes: Int {
    case success = 200
    case internalServerError = 500
    case badRequest = 400
}
