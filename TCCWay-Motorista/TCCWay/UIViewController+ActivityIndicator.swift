//
//  UIViewController+ActivityIndicator.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 9/19/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import  UIKit

extension UIViewController {
    func showLoading() {
        // Hierarchy
        let loadingView = LoadingView(frame: .zero, tag: 100)
        view.addSubview(loadingView)
        view.bringSubview(toFront: loadingView)
        
        // Layout
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            loadingView.widthAnchor.constraint(equalToConstant: 40),
            loadingView.heightAnchor.constraint(equalToConstant: 40),
            loadingView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loadingView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ])
        
        // Animation
        loadingView.startAnimating()
        
        UIView.animate(withDuration: 0.3) {
            loadingView.alpha = 1.0
        }
    }
    
    func hideLoading() {
        for view in view.subviews where view.tag == 100 {
            UIView.animate(withDuration: 0.3, animations: {
                view.alpha = 0.0
            }, completion: { _ in
                view.removeFromSuperview()
            })
        }
    }
}

private class LoadingView: UIView {
    
    private let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    init(frame: CGRect, tag: Int) {
        super.init(frame: frame)
        self.tag = tag
        setup()
        layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = UIColor.lightGray
        layer.cornerRadius = 4
        clipsBounds = true
    }
    
    private func layout() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicator)
        
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
            ])
    }
    
    func startAnimating() {
        activityIndicator.startAnimating()
    }
    
}
