//
//  Fipe.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 13/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import ObjectMapper

struct Fipe: Mappable {
    
    private(set) var name = ""
    private(set) var cod = 0
    
    init?(map: Map) {}
    init?(){}
    
    mutating func mapping(map: Map) {
        name <- map["name"]
        cod <- map["id"]
    }
    
}
