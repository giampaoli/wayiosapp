//
//  CreateLineDescriptionViewController+UITableViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 09/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

extension CreateLineDescriptionViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return daysWeek.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "week", for: indexPath) as? DaysOfWeekTableViewCell
        
        cell?.render(dayOfWeek: daysWeek[indexPath.row])
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCollection = tableView.dequeueReusableCell(withIdentifier: "header")
        return headerCollection
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        self.daysWeekBool[indexPath.row] = true
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        cell.accessoryType = .none
        self.daysWeekBool[indexPath.row] = false
    }
    
}
