//
//  LoginService.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 25/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

class LoginService {
    func postLogin(tokenNumber: String, telephone: String, callback: @escaping (Response) -> Void) {
        print("SERVICE - postLogin")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.auth_login;
        let parameter = ["token" : tokenNumber];
        
        print("parameters")
        print(parameter.description)
        
        do {
            let opt = try HTTP.POST(urlString, parameters: parameter, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
}
