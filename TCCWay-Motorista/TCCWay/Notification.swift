//
//  Notification.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 03/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import ObjectMapper

struct Notification: Mappable {
    
    private(set) var title = ""
    private(set) var content = ""
    private(set) var type = ""
    private(set) var date : Date? = nil
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        title <- map["titulo"]
        content <- map["conteudo"]
        type <- map["type"]
        date <- (map["createdAt"], DateTransform())
    }
    
    var icon: UIImage? {
        if let image = UIImage(named: type) {
            return image
        } else {
            return UIImage(named: "ic_notification")
        }
    }
}
