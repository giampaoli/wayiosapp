//
//  MembersTableViewController.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 08/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit
import SwiftyBeaver

class MembersTableViewController: UITableViewController {
    
    var line = Line()
    
    let log = SwiftyBeaver.self
    fileprivate let presenter = MembersPresenter(lineService: LineService())
    
     var members: [User]? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        
//        presenter.getMembers(lineId: line.idLine.description)
        presenter.getMembers(lineId: "291")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return members == nil ? 0 : members!.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "member", for: indexPath) as? StartPassangerTableViewCell
        
        cell?.render(user: members![indexPath.row])
        
        return cell!
    }
}

extension MembersTableViewController: MemberPresenterView {
    
    func startLoading() {
        log.verbose("Start Loading")
    }
    
    func finishLoading() {
        log.verbose("Finish Loading")
    }
    
    func success(users: [User]) {
        log.info(users)
        self.members = users
        tableView.reloadData()
    }
    
    func error(title: String, message: String) {
        log.error(message)
        presentError(title: title, message: message)
    }
    
    private func presentError(title: String, message: String) {
        let errorAlert = Alert()
        
        errorAlert.showWith(
            controller: self,
            title: title,
            message: message
        )
    }
}
