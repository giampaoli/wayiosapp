//
//  ModelFipe.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 13/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//


import ObjectMapper

struct ModelFipe: Mappable {
    
    private(set) var models = [Fipe]()
    
    init?(map: Map) {}
    init?(){}
    
    mutating func mapping(map: Map) {
        models <- map["modelos"]
    }
    
}
