//
//  UIView+CornerRadius.swift
//  TCCWay-Motorista
//
//  Created by Vinicius Simionato on 08/09/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }
        set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return layer.borderColor.flatMap(UIColor.init)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var clipsBounds: Bool {
        get {
            return clipsToBounds
        }
        set {
            clipsToBounds = newValue
        }
    }
    
    func updateFrame(x: CGFloat? = nil, y: CGFloat? = nil, width: CGFloat? = nil, height: CGFloat? = nil) {
        frame = CGRect(
            x: x ?? frame.origin.x,
            y: y ?? frame.origin.y,
            width: width ?? frame.width,
            height: height ?? frame.height
        )
    }
    
}
