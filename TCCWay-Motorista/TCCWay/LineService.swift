//
//  LineService.swift
//  TCCWay
//
//  Created by Vinicius Simionato on 29/08/17.
//  Copyright © 2017 TCC. All rights reserved.
//

import Foundation
import SwiftHTTP

class LineService {
    func getLines(driverId: String, callback: @escaping (Response) -> Void) {
        print("SERVICE - getLines")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.lines_by_driver + driverId
        
        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
    
    func getInputRequests(lineId: String, callback: @escaping (Response) -> Void) {
        print("SERVICE -  getInputRequests")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.input_request_line + lineId
        
        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
    
    func approveInputRequests(inputRequest: InputRequest, callback: @escaping (Response) -> Void) {
        print("SERVICE - approvalInputRequests")
        
        let parameters = [
            "id_solicitacao" : "\(inputRequest.id)",
            "latitude": "\(inputRequest.latitude)",
            "longitude": "\(inputRequest.longitude)"
        ]
        
        print(parameters)
        
        let urlString = APIConstants.getBaseURL() + APIConstants.approve_input_request
        
        do {
            let opt = try HTTP.POST(urlString, parameters: parameters, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
    
    func disapproveInputRequests(inputRequest: InputRequest, callback: @escaping (Response) -> Void) {
        print("SERVICE - approvalInputRequests")
        
        let parameters = [
            "id" : "\(inputRequest.id)"
        ]
        
        let urlString = APIConstants.getBaseURL() + APIConstants.approve_input_request
        
        do {
            let opt = try HTTP.POST(urlString, parameters: parameters, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
    
    func getMembers(lineId: String, callback: @escaping (Response) -> Void) {
        print("SERVICE - getMembers")
        
        let urlString = APIConstants.getBaseURL() + APIConstants.passenger_by_line + lineId
        
        print(urlString)
        
        do {
            let opt = try HTTP.GET(urlString, parameters: nil, headers: nil, requestSerializer: JSONParameterSerializer.init())
            opt.start { response in
                if let err = response.error {
                    print("Response with error: \(err)")
                }
                
                callback(response)
            }
        } catch let error {
            print("Got an error creating the request: \(error)")
        }
    }
}
